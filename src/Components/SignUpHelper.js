import { useState } from "react";

const SignUpHelper = (initialFieldValues, validate) => {
  const [values, setValues] = useState(initialFieldValues);
  const [errors, setErrors] = useState({});

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    const fieldValue = { [name]: value };
    setValues({
      ...values,
      ...fieldValue,
    });
    validate(fieldValue);
  };

  const resetForm = () => {
    setValues({
      ...initialFieldValues,
    });
    setErrors({});
    //setCurrentId(0);
  };

  return {
    values,
    errors,
    setErrors,
    handleInputChange,
    resetForm,
  };
};

export default SignUpHelper;
