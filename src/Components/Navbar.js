import { Notifications } from "@mui/icons-material";
import { AppBar, Badge, Toolbar, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Rectangle from "../images/rounded-logo.png";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import DrawerComponent from "./DrawerComponent";
import { Link } from "react-router-dom";
import HelpIcon from "@mui/icons-material/Help";
import Avatar from "@mui/material/Avatar";
import Chip from "@mui/material/Chip";
import doctor from "../images/doctor.jpg";
import userActions from "../redux/actions/userActions";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: "hidden",
    position: "relative",
    display: "flex",
    width: "100%",
  },
  logo: {
    maxWidth: 40,
    marginRight: "10px",
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },

  logoTitle: {
    fontSize: "20px",
    color: theme.palette.primary.main2,
    textDecoration: "none",
  },

  midbar: {
    display: "flex",
    marginLeft: "180px",
  },
  hometext: {
    color: theme.palette.secondary.main,
    fontSize: "16px",
  },
  midbarText: {
    marginRight: "25px",
    fontSize: "13px",
    color: theme.palette.primary.main2,
    textDecoration: "none",
  },

  rightbar: {
    display: "flex",
    marginLeft: "100px",
  },

  rightbarText: {
    marginRight: "25px",
    fontSize: "13px",
    color: theme.palette.primary.main2,
    textDecoration: "none",
  },

  rightbarTextDonate: {
    padding: "5px",
    borderRadius: "30px",
    backgroundColor: "lightgrey",
    color: theme.palette.primary.main2,
    marginRight: "15px",
    fontSize: "13px",
    textDecoration: "none",
  },

  rightbarText3: {
    marginRight: "25px",
    fontSize: "13px",
    backgroundColor: theme.palette.secondary.main,
    padding: "5px",
    borderRadius: "30px",
    color: theme.palette.primary.main2,
    textDecoration: "none",
  },

  rightbarText2: {
    marginRight: "25px",
    fontSize: "13px",
    color: theme.palette.secondary.main,
    textDecoration: "none",
    padding: "5px",
    borderRadius: "30px",
    backgroundColor: theme.palette.info.main,
  },
  rightbarBadge: {
    marginRight: "25px",
  },
  helpIcon: {
    color: theme.palette.primary.main2,
    marginRight: "25px",
    marginLeft: "25px",
  },
}));

function Navbar(props) {
  const classes = useStyles();
  let history = useHistory();

  // breakpoints
  const themeBreakPoint = useTheme();

  const isMatch = useMediaQuery(themeBreakPoint.breakpoints.down("sm"));

  const { dispatch, loggedIn } = props;
  const logout = () => {
    dispatch(userActions.logout());
    history.push("/");
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          {/* logo and title */}
          <Link to="/">
            <img
              src={Rectangle}
              alt="round rectangle"
              className={classes.logo}
            />
          </Link>
          <Typography variant="h6">
            <Link to="/" className={classes.logoTitle}>
              Let's Talk
            </Link>
          </Typography>

          {isMatch ? (
            <DrawerComponent />
          ) : (
            <>
              {/* mid section */}
              <Typography>
                <div className={classes.midbar}>
                  <Link to="/" className={classes.midbarText}>
                    <h3 className={classes.hometext}>Home</h3>
                  </Link>
                  <Link to="/connect" className={classes.midbarText}>
                    <h3>Connect</h3>
                  </Link>
                  <Link to="/charity" className={classes.midbarText}>
                    <h3>Charity</h3>
                  </Link>
                  <Link to="/about" className={classes.midbarText}>
                    <h3>About us</h3>
                  </Link>
                  <Link
                    to="/counsellorHomepage"
                    className={classes.rightbarText3}
                  >
                    <h3>For Counsellors</h3>
                  </Link>
                </div>
              </Typography>

              {/* right section */}
              <Typography>
                <div className={classes.rightbar}>
                  {/* <Link to="/counsellor" className={classes.rightbarText3}><h3>Counsellor</h3></Link> */}
                  <Link to="/donate" className={classes.rightbarText}>
                    <h3 className={classes.rightbarTextDonate}>Donate</h3>
                  </Link>
                  <Link to="/" className={classes.rightbarBadge}>
                    <Badge badgeContent={2} color="error">
                      <Notifications color="secondary" />
                    </Badge>
                  </Link>
                  {loggedIn ? (
                    <Chip
                      avatar={<Avatar alt="Natacha" src={doctor} />}
                      label="Logout"
                      variant="outlined"
                      onClick={logout}
                    />
                  ) : (
                    <>
                      <Link to="/signup" className={classes.rightbarText}>
                        <h3>Sign Up</h3>
                      </Link>
                      <Link to="/login" className={classes.rightbarText2}>
                        <h3>Login</h3>
                      </Link>
                    </>
                  )}

                  <HelpIcon className={classes.helpIcon} />
                </div>
              </Typography>
            </>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}

function mapStateToProps(state) {
  const { loggedIn } = state.authentication;
  return {
    loggedIn,
  };
}
//export default Navbar;
export default connect(mapStateToProps)(Navbar);
