import React from 'react';
import { makeStyles } from '@mui/styles';
import { InputBase } from '@mui/material';
import { Search } from '@mui/icons-material';

const useStyles = makeStyles((theme) => ({
    search: {
        display: "flex",
        marginTop: "30px",
        marginLeft: "100px",
        borderRadius: "25px",
    },

    searchInput: {
        backgroundColor: "whitesmoke",
        width: "500px",
        height: "50px",
        padding:" 15px",
        border:" none",
        borderTopLeftRadius: "25px",
        borderBottomLeftRadius: "25px",
        [theme.breakpoints.down("sm")]: {
            width: "300px",
            marginLeft: "-50px",
        },
    },

    searchIcon: {
        backgroundColor: "whitesmoke",
        width: "100px",
        height: "20px",
        padding:" 13px",
        border:" none",
        borderTopRightRadius: "25px",
        borderBottomRightRadius: "25px",
    },
}))

function SearchBar() {
    const classes = useStyles();

    return (
        <div className={classes.search}>
            <InputBase placeholder="Search..." className={classes.searchInput} />
            <Search className={classes.searchIcon} />
        </div>
    )
}

export default SearchBar;
