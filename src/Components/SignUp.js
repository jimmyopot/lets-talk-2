import { React } from "react";
import { makeStyles } from "@mui/styles";
import userActions from "../redux/actions/userActions";
import { connect } from "react-redux";
import { CircularProgress } from "@mui/material";
//import history from "../redux/storeDetails/history";
import { useHistory } from "react-router-dom";
import SignUpHelper from "../Components/SignUpHelper";
import { useToasts } from "react-toast-notifications";

import {
  Paper,
  Grid,
  TextField,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Button,
  Typography,
} from "@mui/material";
import Avatar from "@mui/material/Avatar";
import doctor from "../images/doctor.jpg";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: "50px",
    padding: "20px",
    height: "400px",
    width: "300px",
    margin: "20px auto",
  },
}));

const initialFieldValues = {
  userName: "",
  email: "",
  password: "",
  confirmPassword: "",
};

function SignUp(props) {
  const classes = useStyles();
  const { registering } = props;
  let history = useHistory();
  //toast msg.
  const { addToast } = useToasts();

  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("userName" in fieldValues)
      temp.userName = fieldValues.userName ? "" : "This field is required.";
    if ("password" in fieldValues)
      temp.password = fieldValues.password ? "" : "This field is required.";
    if ("confirmPassword" in fieldValues)
      temp.confirmPassword =
        fieldValues.confirmPassword === values.password
          ? ""
          : "Password & Comfirm Don't Match.";
    if ("email" in fieldValues)
      temp.email = /^$|.+@.+..+/.test(fieldValues.email)
        ? ""
        : "Email is not valid.";
    setErrors({
      ...temp,
    });
    if (fieldValues === values)
      return Object.values(temp).every((x) => x === "");
  };

  const { values, errors, setErrors, handleInputChange, resetForm } =
    SignUpHelper(initialFieldValues, validate);

  const handleSubmit = (event) => {
    event.preventDefault();

    const onSuccess = () => {
      resetForm();
      addToast("Registered successfully, you can now Login", {
        appearance: "success",
      });
      history.push("/login");
    };
    const onFailure = () => {
      // resetForm();
      addToast(
        "An Error Occured while sending to backend, Please try again later.",
        { appearance: "error" }
      );
    };
    const { dispatch } = props;
    if (validate()) {
      console.log("Form is Valid.");
      dispatch(userActions.register(values, onSuccess, onFailure));
    } else {
      console.log("not valid");
      addToast("Not all form fields are valid, Check and try again.", {
        appearance: "error",
      });
    }
  };

  return (
    <div>
      <Grid>
        <Paper elevation={10} className={classes.paper}>
          <Grid align="center">
            <Avatar
              alt="Remy Sharp"
              src={doctor}
              sx={{ width: 40, height: 40 }}
            />
            <h2>Sign Up</h2>
          </Grid>
          <form noValidate onSubmit={handleSubmit}>
            <TextField
              id="standard-basic"
              label="User Name"
              variant="standard"
              fullWidth
              required
              name="userName"
              value={values.userName}
              onChange={handleInputChange}
              {...(errors.userName && {
                error: true,
                helperText: errors.userName,
              })}
            />
            <TextField
              id="standard-basic"
              label="Email"
              variant="standard"
              fullWidth
              required
              name="email"
              value={values.email}
              onChange={handleInputChange}
              {...(errors.email && { error: true, helperText: errors.email })}
            />
            <TextField
              id="standard-basic"
              label="Password"
              variant="standard"
              type="password"
              fullWidth
              required
              name="password"
              value={values.password}
              onChange={handleInputChange}
              {...(errors.password && {
                error: true,
                helperText: errors.password,
              })}
            />
            <TextField
              id="standard-basic"
              label="Confirm Password"
              variant="standard"
              type="password"
              fullWidth
              required
              name="confirmPassword"
              value={values.confirmPassword}
              onChange={handleInputChange}
              {...(errors.confirmPassword && {
                error: true,
                helperText: errors.confirmPassword,
              })}
            />
            <FormGroup>
              <FormControlLabel
                control={<Checkbox style={{ color: "blue" }} />}
                label="I accept the terms and conditions"
              />
            </FormGroup>
            <Button
              type="submit"
              variant="contained"
              fullWidth
              style={{
                backgroundColor: "gray",
                color: "white",
                marginBottom: "10px",
              }}
            >
              {registering ? (
                <CircularProgress style={{ float: "right" }} />
              ) : (
                <span>Sign Up</span>
              )}
            </Button>
          </form>
          <Typography component="div">
            <h5 style={{ color: "gray" }}>Do you have an account?</h5>
            <Link
              to="/login"
              style={{
                color: "blue",
                fontSize: "14px",
                textDecoration: "none",
              }}
            >
              Login
            </Link>
          </Typography>
        </Paper>
      </Grid>
    </div>
  );
}

function mapStateToProps(state) {
  const { registering } = state.registration;
  return {
    registering,
  };
}
export default connect(mapStateToProps)(SignUp);
