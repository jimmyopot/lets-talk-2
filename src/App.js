import "./App.css";
import Navbar from "./Components/Navbar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Grid } from "@mui/material";
import SearchBar from "./Components/SearchBar";
import Home from "./pages/index/Home";
import MySessions from "./pages/home/MySessions";
import CounsellorApplicationForm from "./pages/counsellors/CounsellorApplicationForm";
import { connect } from "react-redux";
import history from "./redux/storeDetails/history";
import { alertActions } from "./redux/actions";
import { PrivateRoute } from "./Components/PrivateRoute";
import ModalPopup from "./pages/counsellors/CounsellorModalPopup";
import SignUpHome from "./pages/signForms/SignUpHome";
import LoginHome from "./pages/signForms/LoginHome";
import NavbarCounsellor from "./pages/counsellors/components/NavbarCounsellor";
import LandingPage from "./pages/counsellors/LandingPage";
import NavbarCounsellorAdmin from "./pages/counsellors/components/NavbarCounsellorAdmin";
import AdminPanel from "./pages/counsellors/AdminPanel";
import CounsellorSignUpHome from "./pages/signForms/CounsellorSignUpHome";
import CounsellorLoginHome from "./pages/signForms/CounsellorLoginHome";
import FilterHomePage from "./pages/counsellors/FilterHomePage";
import DataTable from "./pages/counsellors/DataTableInfo";
import ViewProfileCounsellor from "./pages/counsellors/ViewProfileCounsellor";
import BookAppointment from "./pages/counsellors/BookAppointment";
import BookCalendar from "./pages/counsellors/BookCalendar";
// import UploadFiles from "./pages/fileUploader/components/uploadFilesComponent";


function App(props) {
  const { dispatch } = props;
  history.listen((location, action) => {
    // clear alert on location change
    dispatch(alertActions.clear());
  });

  return (
    <Router history={history}>
      <div className="App">
        <Switch>
          <Route path="/faq">
            <h1>FAQ</h1>
          </Route>
          <Route path="/signup">
            <Navbar />
            <SignUpHome />
          </Route>
          <Route path="/login">
            <Navbar />
            <LoginHome />
          </Route>
          <Route path="/donate">
            <h1>Donate</h1>
          </Route>
          <Route path="/counsellorHomepage">
            {/* <NavbarCounsellor /> */}
            <ModalPopup />
          </Route>
          <Route path="/counsellorLandingpage">
            <NavbarCounsellor />
            <LandingPage />
          </Route>
          <Route path="/counsellorAdminPanel">
            <NavbarCounsellorAdmin />
            <AdminPanel />
          </Route>
          <Route path="/counsellorApplicationForm">
            <Navbar />
            <CounsellorApplicationForm />
          </Route>
          <Route path="/counsellorFilterPage">
            <Navbar />
            <FilterHomePage />
          </Route>
          <Route path="/counsellorDataTable">
            <Navbar />
            <DataTable />
          </Route>
          <Route path="/counsellorSignup">
            <NavbarCounsellorAdmin />
            <CounsellorSignUpHome />
          </Route>
          <Route path="/counsellorLogin">
            <NavbarCounsellor />
            <CounsellorLoginHome />
          </Route>
          <Route path="/viewCounsellorProfile">
            <NavbarCounsellor />
            <ViewProfileCounsellor />
          </Route>
          <Route path="/bookAppointment">
            <NavbarCounsellor />
            <BookAppointment />
          </Route>
          {/* <Route path="/fileuploads">
            <Navbar />
            <CounsellorLogin />
          </Route> */}
          <Route path="/bookCalendar">
            <BookCalendar />
          </Route>

          <Route path="/getstarted">
            <h1>Get Started</h1>
          </Route>
          <Route path="/about">
            <h1>About Us</h1>
          </Route>
          <Route path="/charity">
            <h1>Charity</h1>
          </Route>
          <Route path="/connect">
            <h1>Connect</h1>
          </Route>
          <PrivateRoute exact path="/mysessions">
            <Navbar />
            <MySessions />
          </PrivateRoute>
          <Route exact path="/">
            <Navbar />
            <Grid container>
              <Grid item xs={12}>
                <SearchBar />
              </Grid>
            </Grid>
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

// export default App;
function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert,
  };
}

export default connect(mapStateToProps)(App);
