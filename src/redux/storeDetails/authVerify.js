import { withRouter } from "react-router-dom";

function AuthVerify(props) {
  props.history.listen(() => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      if (user.expiration * 1000 < Date.now()) {
        props.logOut();
      }
    }
  });
}

export default withRouter(AuthVerify);
