import { userConstants } from "../constants";
import { userService } from "../services";
import { alertActions } from "./";

const userActions = {
  login,
  logout,
  register,
};

function login(username, password, onSuccess, onFailure) {
  return (dispatch) => {
    dispatch(request({ username }));

    userService
      .login(username, password)
      .then(
        (user) => {
          dispatch(success(user));
          onSuccess();
        },
        (error) => {
          dispatch(failure(error));
          onFailure();
          dispatch(alertActions.error(error));
        }
      )
      .catch((err) => {
        console.log(err);
        dispatch(failure(err));
        onFailure();
        dispatch(alertActions.error(err));
      });
  };

  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}

function register(user, onSuccess, onFailure) {
  return (dispatch) => {
    dispatch(request(user));

    userService.register(user).then(
      () => {
        dispatch(success());
        //history.push("/login");
        onSuccess();
        dispatch(alertActions.success("Registration successful"));
      },
      (error) => {
        dispatch(failure(error));
        onFailure();
        dispatch(alertActions.error(error));
      }
    );
  };

  function request(user) {
    return { type: userConstants.REGISTER_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.REGISTER_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.REGISTER_FAILURE, error };
  }
}
export default userActions;
