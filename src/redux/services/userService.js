import { config } from "../storeDetails";
import axios from "axios";

export const userService = {
  login,
  logout,
  register,
};

function login(username, password) {
  return axios
    .post(config.apiUrl + "account/signin", {
      username,
      password,
    })
    .then((response) => {
      if (response.data.token) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }

      return response.data;
    });
}

function register(user) {
  return axios.post(config.apiUrl + "account/signup", user);
}

function logout(logoutHandler) {
  // remove user from local storage to log user out
  localStorage.removeItem("user");
}
