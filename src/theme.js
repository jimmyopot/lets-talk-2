import { createTheme } from '@mui/material/styles';
import { grey } from '@mui/material/colors';

const theme = createTheme({
  palette: {
    primary: {
      main: grey[200],
      main2: grey[600],
    },  
    secondary: {
      main: '#70D8FF',  // light blue
    },
    error: {
      main: '#EEB775',  //orange
    },
    // warning: {},
    info: {
      main: grey[600],
    },
    // success: {},
  },
});

export default theme;