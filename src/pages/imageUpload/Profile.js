import React, { useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Button, TextField } from '@mui/material';

const defaultImageSrc = 'img/grieving.jpg';

const initialFieldValues = {
    profileId: 0,
    profileName: '',
    occupation: '',
    imageName: '',
    imageSrc: defaultImageSrc,
    imageFile: null,
}

const useStyles = makeStyles((theme) => ({
    form : {
        display: "flex",
        flexDirection: "column",
        margin: "20px",
    },
}))

function Profile(props) {
    const {addOrEdit} = props

    const classes = useStyles();
    const [values, setValues] = useState(initialFieldValues);
    const [errors, setErrors] = useState({});

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        })
    }

    const showPreview = (e) => {
        if (e.target.files && e.target.files[0]) {
            let imageFile = e.target.files[0];
            const reader = new FileReader();
            reader.onload = (x) => {
                setValues({
                    ...values,
                    imageFile,
                    imageSrc: x.target.result
                })
            }
            reader.readAsDataURL(imageFile);
        }
        else {
            setValues({
                ...values,
                imageFile: null,
                imageSrc: defaultImageSrc
            })
        }
    }

    const validate = () => {
        let temp = {};
        temp.profileName = values.profileName === "" ? false : true;
        temp.imageSrc = values.imageSrc === defaultImageSrc ? false : true;
        setErrors(temp);
        return Object.values(temp).every(x => x === true)
    }

    // const resetForm = () => {
    //     setValues(initialFieldValues)
    //     document.getElementById('image-uploader').value = null;
    //     setErrors({});
    // }

    const handleFormSubmit = (e) => {
        e.preventDefault()
        if(validate()) {
            const formData = new FormData();
            formData.append('profileId', values.profileId)
            formData.append('profileName', values.profileName)
            formData.append('occupation', values.occupation)
            formData.append('imageName', values.imageName)
            formData.append('imageFile', values.imageFile)
            addOrEdit(formData.resetForm)
        }
    }

    const applyErrorClass = (field) => ((
        field in errors && errors[field] === false
    ) ? 'invalid-field' : '')

    return (
        <>
            <div className={classes.root}>
                <div className={classes.header}>
                    <p>Profile Upload</p>
                </div>
                <form onSubmit={handleFormSubmit} className={classes.form}>
                    <img src={values.imageSrc} alt="" style={{marginBottom: "20px"}} />
                    <TextField 
                        type="file"
                        accept="image/"
                        name="profileName"
                        id="image-uploader"
                        onChange={showPreview}
                        className={applyErrorClass('imageSrc')}
                        style={{marginBottom: "10px"}}
                    />
                    <TextField 
                        className=""
                        placeholder="Profile Name"
                        name="profileName"
                        value={values.profileName}
                        onChange={handleInputChange}
                        // className={applyErrorClass('employeeName')}
                        style={{marginBottom: "10px"}}
                    />
                    <TextField 
                        className=""
                        placeholder="Occupation"
                        name="occupation"
                        value={values.occupation}
                        onChange={handleInputChange}
                        style={{marginBottom: "10px"}}
                    />
                    <Button variant="contained" style={{backgroundColor: "lightblue"}}>Submit</Button>
                </form>
            </div>
        </>
    )
}

export default Profile
