import React from 'react';
import { Grid } from '@mui/material';
import Profile from './Profile';
import axios from "axios";

function ImageUpload2() {
    const profileAPI = (url = '') => {
        return {
            fetchAll: () => axios.get(url),
            create: newRecord => axios.post(url, newRecord),
            update: (id, updateRecord) => axios.put(url + id, updateRecord),
            delete: id => axios.delete(url + id)
        }
    }

    const addOrEdit = (formData, onSuccess) => {
        profileAPI().create(formData)
        .then(res => {
            onSuccess();
        })
        .catch(err => console.log(err));
    }

    return (
        <div>
            <Grid container>
                <Grid item sm={4}>
                    <Profile 
                        addOrEdit={addOrEdit}
                    />
                    <h1>Left side</h1>
                </Grid>
                <Grid item sm={8}>
                    {/* <Feed /> */}
                    <h1>Right side</h1>
                </Grid>
                
            </Grid>
        </div>
    )
}

export default ImageUpload2
