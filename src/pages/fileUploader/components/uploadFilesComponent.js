// import { Button, LinearProgress, ListItem, Typography } from "@mui/material";
// import { Box } from "@mui/system";
// import { makeStyles } from '@mui/styles';
// import React, { Component, useState, useEffect } from "react";
// import uploadFilesService from "../services/uploadFilesService";


// const BorderLinearProgress = makeStyles((theme) => ({
//     root: {
//       height: 15,
//       borderRadius: 5,
//     },
//     colorPrimary: {
//       backgroundColor: "#EEEEEE",
//     },
//     bar: {
//       borderRadius: 5,
//       backgroundColor: '#1a90ff',
//     },
//   }))(LinearProgress);

//   function UploadFiles() {
//       const [selectFiles, setSelectFiles] = useState('');
//       const [currentFiles, setCurrentFiles] = useState('');
//       const [progress, setProgress] = useState(0);
//       const [message, setMessage] = useState('');
//       const [isError, setisError] = useState(false);
//       const [fileInfos, setFileInfos] = useState([]);

//       useEffect(() => {
//         uploadFilesService.getFiles().then((response) => {
//             setFileInfos({
//             fileInfos: response.data,
//             });
//             // setFileInfos(response.data);
//         }); 
//       }, []);

//       const selectFile = (event) => {
//         setSelectFiles({
//           selectedFiles: event.target.files,
//         });
//         // setSelectFiles(event.target.files)
//       };

//       const upload = () => {
//         let currentFile = selectedFiles[0];
    
//         this.setState({
//           progress: 0,
//           currentFile: currentFile,
//         });
    
//         uploadFilesService.upload(currentFile, (event) => {
//           this.setState({
//             progress: Math.round((100 * event.loaded) / event.total),
//           });
//         })
//           .then((response) => {
//             this.setState({
//               message: response.data.message,
//               isError: false
//             });
//             return uploadFilesService.getFiles();
//           })
//           .then((files) => {
//             this.setState({
//               fileInfos: files.data,
//             });
//           })
//           .catch(() => {
//             this.setState({
//               progress: 0,
//               message: "Could not upload the file!",
//               currentFile: undefined,
//               isError: true
//             });
//           });
    
//         this.setState({
//           selectedFiles: undefined,
//         });
//       }

//   }
  
//   export default class UploadFiles extends Component {
//     constructor(props) {
//       super(props);
//       this.selectFile = this.selectFile.bind(this);
//       this.upload = this.upload.bind(this);
  
//       this.state = {
//         selectedFiles: undefined,
//         currentFile: undefined,
//         progress: 0,
//         message: "",
//         isError: false,
//         fileInfos: [],
//       };
//     }
  
//     componentDidMount() {
//       uploadFilesService.getFiles().then((response) => {
//         this.setState({
//           fileInfos: response.data,
//         });
//       });
//     }
  
//     selectFile(event) {
//       this.setState({
//         selectedFiles: event.target.files,
//       });
//     }
  
//     upload() {
//       let currentFile = this.state.selectedFiles[0];
  
//       this.setState({
//         progress: 0,
//         currentFile: currentFile,
//       });
  
//       uploadFilesService.upload(currentFile, (event) => {
//         this.setState({
//           progress: Math.round((100 * event.loaded) / event.total),
//         });
//       })
//         .then((response) => {
//           this.setState({
//             message: response.data.message,
//             isError: false
//           });
//           return uploadFilesService.getFiles();
//         })
//         .then((files) => {
//           this.setState({
//             fileInfos: files.data,
//           });
//         })
//         .catch(() => {
//           this.setState({
//             progress: 0,
//             message: "Could not upload the file!",
//             currentFile: undefined,
//             isError: true
//           });
//         });
  
//       this.setState({
//         selectedFiles: undefined,
//       });
//     }
  
//     render() {
//       const {
//         selectedFiles,
//         currentFile,
//         progress,
//         message,
//         fileInfos,
//         isError
//       } = this.state;
      
//       return (
//         <div className="mg20">
//           {currentFile && (
//             <Box className="mb25" display="flex" alignItems="center">
//               <Box width="100%" mr={1}>
//                 <BorderLinearProgress variant="determinate" value={progress} />
//               </Box>
//               <Box minWidth={35}>
//                 <Typography variant="body2" color="textSecondary">{`${progress}%`}</Typography>
//               </Box>
//             </Box>)
//           }
  
//           <label htmlFor="btn-upload">
//             <input
//               id="btn-upload"
//               name="btn-upload"
//               style={{ display: 'none' }}
//               type="file"
//               onChange={this.selectFile} />
//             <Button
//               className="btn-choose"
//               variant="outlined"
//               component="span" >
//                Choose Files
//             </Button>
//           </label>
//           <div className="file-name">
//           {selectedFiles && selectedFiles.length > 0 ? selectedFiles[0].name : null}
//           </div>
//           <Button
//             className="btn-upload"
//             color="primary"
//             variant="contained"
//             component="span"
//             disabled={!selectedFiles}
//             onClick={this.upload}>
//             Upload
//           </Button>
  
//           <Typography variant="subtitle2" className={`upload-message ${isError ? "error" : ""}`}>
//             {message}
//           </Typography>
  
//           <Typography variant="h6" className="list-header">
//             List of Files
//             </Typography>
//           <ul className="list-group">
//             {fileInfos &&
//               fileInfos.map((file, index) => (
//                 <ListItem
//                   divider
//                   key={index}>
//                   <a href={file.url}>{file.name}</a>
//                 </ListItem>
//               ))}
//           </ul>
//         </div >
//       );
//     }
//   }