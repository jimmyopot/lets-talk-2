import React from 'react';
import { makeStyles } from '@mui/styles';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import vectorchat from '../../images/vectorchat.jpg'
import { InputBase } from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    bannercontainer: {
        display: "flex",
        marginTop: "30px",
        marginBottom: "30px",
        marginLeft: "10px",
        height: "250px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            height: "800px",
        },
    },
    cardleft: {
        display: "flex",
        width: "200px",
        paddingTop: "20px",
        [theme.breakpoints.down("sm")]: {
            backgroundColor: "white",
            width: "380px",
            marginBottom: "10px",
            height: "40vh",
        },
    },
    cardmedia: {
        minWidth: 180,
        marginTop: "20px",
        marginLeft: "0px",
        marginRight: "200px",
        height: "180px",
        padding: "10px",
        [theme.breakpoints.down("sm")]: {
            marginTop: "-20px",
            height: "150px",
            minWidth: 350,
        },
    },
    cardmid: {
        [theme.breakpoints.down("sm")]: {
            width: "380px",
            marginBottom: "10px",
            height: "70vh",
            paddingTop: "20px",
        },
    },
    heading: {
        color: theme.palette.error.main,
        fontSize: "50px",
    },
    headingh2: {
        [theme.breakpoints.down("sm")]: {
            color: "orange",
            fontSize: "20px",
            marginLeft: "15px",
        },
    },
    search1: {
        display: "flex",
        marginTop: "20px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            
        },
    },
    search: {
        display: "flex",
        marginRight: "10px",
    },
    searchInput1: {
        backgroundColor: "white",
        width: "180px",
        height: "40px",
        padding:" 10px",
        border:" none",
        marginBottom: "10px",
        borderTopLeftRadius: "25px",
        borderBottomLeftRadius: "25px",
        [theme.breakpoints.down("sm")]: {
            width: "300px", 
            marginBottom: "30px",
        },
    },
    searchInput2: {
        backgroundColor: "gray",
        width: "450px",
        height: "40px",
        padding:" 15px",
        border:" none",
        borderRadius: "25px",
        color: "white", 
        alignItem: "center",
        cursor: "pointer",
        [theme.breakpoints.down("sm")]: {
            width: "340px", 
            marginBottom: "30px",
        },
    },
    dropDown1: {
        backgroundColor: "white",
        width: "20px",
        height: "10px",
        padding:" 8px",
        border:" none",
        borderTopRightRadius: "25px",
        borderBottomRightRadius: "25px",
    },
    instantMatchContainer: {
        display: "flex",
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },

    },
    arrowForward: {
        marginTop: "5px",
        borderRadius: "25px",
        marginLeft: "5px",
        backgroundColor: "lightgray",
    },
    cardright: {
        maxWidth: 350,
        [theme.breakpoints.down("sm")]: {
            width: "400px",
            marginBottom: "20px",
            height: "60vh",
        },
    },
    cardrightBackground: {
        backgroundColor: "#fff", 
        marginTop: "10px",
    },
    cardrightHeader: {
        fontSize: "17px", 
        color: theme.palette.secondary.main, 
        padding: "5px"
    },
    cardrightHeader1: {
        // fontSize: "17px", 
        color: theme.palette.primary.main2, 
        // padding: "5px"
    },
}))

function Banner1() {
    const classes = useStyles();

    return (
        <div className={classes.bannercontainer}>
            <Card className={classes.cardleft} style={{backgroundColor: "whitesmoke"}}>
                <CardMedia
                    component="img"
                    image={vectorchat}
                    alt="counsellor"
                    className={classes.cardmedia}
                />
            </Card>

            <Card className={classes.cardmid} style={{backgroundColor: "whitesmoke"}}>
                <CardContent>
                    <Typography className={classes.heading}>
                        <h2 className={classes.headingh2}>Find the right counsellor for you</h2>
                    </Typography>
                    <Typography>
                        <div className={classes.search1}>
                            <div className={classes.search}>
                                <InputBase placeholder="What's worrying you?" className={classes.searchInput1} />
                                <ArrowDropDownIcon className={classes.dropDown1} />
                            </div>
                            <div className={classes.search}>
                                <InputBase placeholder="Counsellor" className={classes.searchInput1} />
                                <ArrowDropDownIcon className={classes.dropDown1} />
                            </div>
                        </div>
                        <div className={classes.search2}>
                            {/* <InputBase placeholder="Find a counsellor now" style={{color: "white", alignItem: "center"}} className={classes.searchInput2} /> */}
                            <Link to="/counsellorFilterPage">
                            <button className={classes.searchInput2}>Find a counsellor now</button>
                            </Link>
                        </div>
                    </Typography>
                    <div className={classes.instantMatchContainer} style={{marginLeft: "320px", marginTop: "10px",}}>
                        <h4 className={classes.instantMatch}>Instant match</h4>
                        <ArrowForwardIcon className={classes.arrowForward} style={{fontSize: "15px"}} />
                    </div>
                </CardContent>
            </Card>
            
            <Card className={classes.cardright} style={{backgroundColor: "whitesmoke"}}>
                <CardContent>
                    <h2 className={classes.cardrightHeader1}>Newsfeed</h2>
                    <div className={classes.cardrightBackground}>
                        <h3 className={classes.cardrightHeader}>Changing the culture of counselling</h3>
                        <h5 style={{color: "gray", padding: "5px"}}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu orci non justo dignissim 
                        aliquam vel nec eros. Nunc sollicitudin elit libero, in condimentum augue consectetur vitae. 
                        Suspendisse sodales felis at risus scelerisque consectetur. Curabitur ut orci egestas purus
                        </h5>
                        <h5 style={{padding: "5px"}}>Read more...</h5>
                    </div>
                </CardContent>
            </Card>
                
        </div>
    )
}

export default Banner1
