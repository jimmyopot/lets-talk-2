import React from 'react';
import { makeStyles } from '@mui/styles';
import { Card, CardContent, CardMedia } from '@mui/material';

const useStyles = makeStyles((theme) => ({
    root: {
        width: "300px",
        // minHeight: "100px",
        marginTop: "20px",
        marginBottom: "20px",
        marginLeft: "30px",
        [theme.breakpoints.down("sm")]: {
            width: "360px",
            height: "260px",
            marginLeft: "20px",
        },
    },
    heading: {
        color: theme.palette.secondary.main,
        marginBottom: "10px",
        fontSize: "15px",
    },
    datetime: {
        display: "flex",
        marginBottom: "10px",
    },
    date: {
        marginRight: "30px",
        color: "gray",
    },
    time: {
        color: "gray",
    },
    footer: {
        display: "flex",
        marginBottom: "-15px",
    },
    name: {
        marginLeft: "10px",
        color: theme.palette.info.main,
        marginTop: "5px",
        fontSize: "13px"
    },
}))

function UpcomingEvents( { image, title, date, time, avatar, name } ) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <CardMedia
                    component="img"
                    height="120"
                    image={image}
                    alt="messages"
                    className={classes.image}
                />
                <CardContent>
                    <div>
                        <h1 className={classes.heading}>{title}</h1>
                    </div>
                    <div className={classes.datetime}>
                        <h5 className={classes.date}>{date}</h5>
                        <h5 className={classes.time}>{time}</h5>
                    </div>
                    <div className={classes.footer}>
                        <p>{avatar}</p>
                        <h4 className={classes.name}>{name}</h4>
                    </div>
                </CardContent>
            </Card>
            
        </div>
    )
}

export default UpcomingEvents
