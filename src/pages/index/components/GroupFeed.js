import React from 'react';
import { makeStyles } from '@mui/styles';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';

const useStyles = makeStyles((theme) => ({
    card: {
        display: "flex",
        width: "380px",
        height: "210px",
        marginLeft: "40px",
        marginTop: "30px",
        [theme.breakpoints.down("sm")]: {
            width: "380px",
            height: "180px",
            marginLeft: "20px",
            marginTop: "20px",
            marginBottom: "10px",
        },
    },
    image: {
        paddingTop: "10px",
        paddingLeft: "5px",
    },
    heading: {
        fontSize: "15px",
        color: theme.palette.secondary.main,
        fontWeight: "bold",
    },
    headingtitle: {
        [theme.breakpoints.down("sm")]: {
            fontSize: "11px",
            color: "skyblue",
        },
    },
    bodytext: {
        color: "gray",
        backgroundColor: "whitesmoke",
    },
    bodytextp: {
        [theme.breakpoints.down("sm")]: {
            fontSize: "12px",
            color: "gray",
        },
    },
    readmore: {
        color: "gray",
        backgroundColor: "whitesmoke",
    },
}))

function GroupFeed( { title, image, text } ) {
    const classes = useStyles();
    
    return (
        <Card className={classes.card}>
            <CardMedia
                component="img"
                height="140"
                image={image}
                alt="messages"
                className={classes.image}
            />
            <CardContent>
                <Typography gutterBottom component="div" className={classes.heading}>
                    <p className={classes.headingtitle}>{title}</p>
                </Typography>
                <Typography component="div" className={classes.bodytext} style={{ fontSize: "13px" }}>
                    <p className={classes.bodytextp}>{text}</p>
                </Typography>
                <Typography className={classes.readmore} style={{ fontSize: "15px" }}>
                    Read more...
                </Typography>
            </CardContent>
        </Card>
    )
}

export default GroupFeed
