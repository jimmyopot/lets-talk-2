import React from 'react'
import { makeStyles } from '@mui/styles';
import { Card, CardContent, CardMedia, Typography } from '@mui/material';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
        marginBottom: "30px",
        display: "flex",
        [theme.breakpoints.down("sm")]: {
            width: "400px",
            height: "180px",
            marginLeft: "-20px",
        },
    },
    card: {
        display: "flex",
        width: "380px",
        height: "190px",
        marginLeft: "40px",
    },
    image: {
        paddingTop: "10px",
        paddingLeft: "5px",
        width: "100%",
        height: "150px",
        // borderRadius: "50px",
        [theme.breakpoints.down("sm")]: {
            paddingTop: "30px",
            paddingLeft: "5px",
            width: "50px",
            height: "120px",
        },
    },
    heading: {
        fontSize: "15px",
        color: theme.palette.secondary.main,
        fontWeight: "bold",
        [theme.breakpoints.down("sm")]: {
            color: "orange",
            fontSize: "13px",
        },
    },
    bodytext: {
        color: theme.palette.info.main,
        fontSize: "13px",
        [theme.breakpoints.down("sm")]: {
            color: "gray",
            fontSize: "12px",
        },
    },
    about: {
        fontSize: "12px",
        [theme.breakpoints.down("sm")]: {
            fontSize: "11px",
        },
    }
}))

function Spotlight( { title, image, text, about } ) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <CardMedia
                    component="img"
                    height="140"
                    image={image}
                    alt="messages"
                    className={classes.image}
                />
                <CardContent>
                    <Typography>
                        <p className={classes.heading}>{title}</p>
                    </Typography>
                    <Typography>
                        <p className={classes.bodytext}>{text}</p>
                    </Typography>
                    <Typography>
                        <p className={classes.about}>{about}</p>
                    </Typography>
                </CardContent>
            </Card>
            
        </div>
    )
}

export default Spotlight
