import React from 'react'
import { makeStyles } from '@mui/styles';
import { Card, Container } from '@mui/material';
import { Link } from 'react-router-dom';
import Rectangle from "../../images/rounded-logo.png";
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookIcon from '@mui/icons-material/Facebook';
import YouTubeIcon from '@mui/icons-material/YouTube';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "60px",
        [theme.breakpoints.down("sm")]: {
            marginTop: "20px",
        },
    },
    cardtop: {
        display: "flex",
        justifyContent: "space-between",
        backgroundColor: "green",
        height: "60px",
        paddingTop: "40px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "row",
            width: "420px",
            height: "30px",
            marginLeft: "-20px",
            marginBottom: "0px",
        },
    },
    contentleft: {
        display: "flex",
        marginLeft: "50px",
    },
    logo: {
        width: "35px",
        cursor: "pointer",
        marginRight: "10px",
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
    logoTitle: {
        marginTop: "20px",
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
    contentright: {
        display: "flex",
    },
    contentrightLink: {
        marginRight: "50px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "40px",
            marginLeft: "-15px",
            marginTop: "-15px",
        },
    },

    // bottom section
    cardbottom: {
        display: "flex",
        justifyContent: "space-between",
        backgroundColor: "green",
        height: "60px",
        paddingTop: "20px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            width: "420px",
            height: "100px",
            marginLeft: "-20px",
            marginBottom: "0px",
        },
    },
    contentleftmedia: {
        display: "flex",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "row",
            width: "420px",
            marginLeft: "130px",
            marginBottom: "10px",
        },
    },
    socialMedia1: {
        marginLeft: "50px",
        [theme.breakpoints.down("sm")]: {
            marginLeft: "0px",
        },
    },
    contentrightbottom: {
        display: "flex",
        [theme.breakpoints.down("sm")]: {
            marginBottom: "10px",
        },
    },
    contentrightLinkbottom: {
        marginRight: "30px",
        [theme.breakpoints.down("sm")]: {
            marginTop: "-50px",
            marginLeft: "20px",
            marginRight: "10px",
        },
    },
    socialMedia: {
        marginLeft: "20px",
    },
    iconlink1: {
        color: "blue", 
        fontSize: 50,
    },
    iconlink2: {
        color: "skyblue", 
        fontSize: 50,
    },
    iconlink3: {
        color: "red", 
        fontSize: 50,
    },
    iconlink4: {
        color: "blue", 
        fontSize: 50,
    },
    copyright: {
        backgroundColor: "whitesmoke",
        height: "60px",
        [theme.breakpoints.down("sm")]: {
            marginTop: "0px",
            marginBottom: "10px",
            width: "420px",
            height: "60px",
            marginLeft: "-20px",
        },
    },
    copy: {
        marginLeft: "50px", 
        color: "gray",
        [theme.breakpoints.down("sm")]: {
            paddingTop: "20px",
            marginLeft: "100px",
        },
    },
}))

function Footer() {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            <Card className={classes.cardtop} style={{backgroundColor: "whitesmoke"}}>
                
                <div className={classes.contentleft}>
                    <Link to="/">
                        <img src={Rectangle} alt="round rectangle" className={classes.logo} />
                    </Link>
                    <div>
                        <Link to="/" className={classes.logoTitle} style={{color: "blue", fontSize: "17px", textDecoration: "none", fontWeight: "500", paddingTop: "50px"}}>
                            Let's Talk
                        </Link>
                    </div>
                </div>
            
                <div className={classes.contentright}>
                    <div className={classes.contentrightLink}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>About Us</Link>
                    </div>
                    <div className={classes.contentrightLink}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Donate</Link>
                    </div>
                    <div className={classes.contentrightLink}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Charity</Link>
                    </div>
                    <div className={classes.contentrightLink}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Help</Link>
                    </div>
                    <div className={classes.contentrightLink}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Support</Link>
                    </div>
                </div>
            </Card>

            <Card className={classes.cardbottom} style={{backgroundColor: "whitesmoke"}}>
                
                <div className={classes.contentleftmedia}>
                    <div className={classes.socialMedia1}>
                        <Link to="/"><FacebookIcon className={classes.iconlink1} /></Link>
                    </div>
                    <div className={classes.socialMedia}>
                        <Link to="/"><TwitterIcon className={classes.iconlink2} /></Link>
                    </div>
                    <div className={classes.socialMedia}>
                        <Link to="/"><YouTubeIcon className={classes.iconlink3} /></Link>
                    </div>
                    <div className={classes.socialMedia}>
                        <Link to="/"><LinkedInIcon className={classes.iconlink4} /></Link>
                    </div>
                </div>
            
                <div className={classes.contentrightbottom}>
                    <div className={classes.contentrightLinkbottom}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Data Privacy</Link>
                    </div>
                    <div className={classes.contentrightLinkbottom}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Legal Terms</Link>
                    </div>
                    <div className={classes.contentrightLinkbottom}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Cookies Policy</Link>
                    </div>
                    <div className={classes.contentrightLinkbottom}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Privacy Policy</Link>
                    </div>
                    <div className={classes.contentrightLinkbottom}>
                        <Link to="/about" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}}>Terms and Conditions</Link>
                    </div>
                </div>    
                
            </Card>

            <div className={classes.copyright}>
                <h5 className={classes.copy}>Copyright @ 2021 site by 
                <Link to="/" style={{textDecoration: "none", color: "blue"}}> Let's Talk</Link></h5>
                    
            </div>
            
        </Container>
    )
}

export default Footer
