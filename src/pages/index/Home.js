import React from 'react';
import GroupFeed from './components/GroupFeed';
import Banner1 from './Banner1';
import Grid from '@mui/material/Grid';
import { makeStyles } from '@mui/styles';
import { Avatar, Container } from '@mui/material';
import Banner2 from './Banner2';
import Banner3 from './Banner3';
import Spotlight from './components/Spotlight';
import { Link } from 'react-router-dom';
import UpcomingEvents from './components/UpcomingEvents';
import Footer from './Footer';
import messages from '../../images/messages.jpg'
import unsplashman from '../../images/unsplashman.jpg'
import unsplashwoman from '../../images/unsplashwoman.jpg'
import grouptherapy from '../../images/grouptherapy.jpg'
import metoo from '../../images/metoo.jpg'
import sad from '../../images/sad.jpg'
import doctor from '../../images/doctor.jpg'
import hope from '../../images/hope.jpg'


const useStyles = makeStyles((theme) => ({
    gridcontainer: {
        paddingRight: "50px",
    },
    rightText: {
        color: "orange",
        fontSize: "15px",
        marginTop: "100px",
        marginLeft: "50px",
        [theme.breakpoints.down("sm")]: {
            fontSize: "15px",
            marginTop: "20px",
            marginLeft: "50px",
        },
    },
    rightTextName: {
        marginLeft: "50px",
        fontSize: "13px",
        marginTop: "5px",
        color: "lightblue",
    },
}))

function Home() {
    const classes = useStyles();

    return (
        <Container>
            <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                    <Banner1 />
                </Grid>
            </Grid>

            <Grid container spacing={1} className={classes.gridcontainer}>
                <Grid item xs={12} sm={6} md={6}>
                    <GroupFeed 
                        title={<h3>When do you need a counsellor?</h3>}
                        image={messages}
                        text={<div>"Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Integer sagittis ullamcorper 
                                elit, sit amet tincidunt dui consectetur sed.
                                Maecenas suscipit."
                            </div>}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                    <h2 className={classes.rightText}>
                        "The curious paradox is that when I accept myself<br />
                        just as I am, then I can change."<br />
                    </h2>
                    <h4 className={classes.rightTextName}>-Carl Rogers</h4>
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                    <GroupFeed 
                        title={<h3>What are the benefits of counselling?</h3>}
                        image={messages}
                        text={<div>"Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Integer sagittis ullamcorper
                                elit, sit amet tincidunt dui consectetur sed.
                                Maecenas suscipit."
                            </div>}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                    <GroupFeed 
                        title={<h3>How to pick the right counsellor.</h3>}
                            image={messages}
                            text={<div>"Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Integer sagittis ullamcorper
                                    elit, sit amet tincidunt dui consectetur sed.
                                    Maecenas suscipit."
                                </div>}
                        />
                </Grid>
            </Grid>

            <Banner2 />

            <Grid container spacing={1} className={classes.gridcontainer}>
                <Grid item xs={12} sm={6} md={6}>
                    <Spotlight
                        title={<h3>Dr. Jordan Peterson</h3>}
                        image={unsplashman}
                        text={<p>"Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Integer sagittis ullamcorper
                                    elit, sit amet tincidunt dui consectetur sed. 
                                    Maecenas suscipit, nisi sed rhoncus consectetur,  
                                    libero ligula euismod nulla, non placerat enim hp"
                            </p>}
                        about={<Link to="/about"><h3>About Jordan Peterson</h3></Link>}
                    />
                </Grid>

                <Grid item xs={12} sm={6} md={6}>
                    <Spotlight
                        title={<h3>Dr. Frank Njenga</h3>}
                        image={unsplashwoman}
                        text={<p>"Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Integer sagittis ullamcorper 
                                    elit, sit amet tincidunt dui consectetur sed.
                                    Maecenas suscipit, nisi sed rhoncus consectetur,  
                                    libero ligula euismod nulla, non placerat enim hp"
                            </p>}
                        about={<Link to="/about"><h3>About Frank Njenga</h3></Link>}
                    />
                </Grid>
            </Grid>
            
            <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                    <Banner3 />
                </Grid>
            </Grid>

            <Grid container spacing={1} className={classes.gridcontainer}>
                <Grid item xs={12} sm={6} md={4}>
                    <UpcomingEvents
                        image={grouptherapy}
                        title={<p>Learning to cope with life's challenges through group therapy</p>}  
                        date={<p>Sep 1, 2021</p>}
                        time={<p>10.30 AM</p>}
                        avatar={<Avatar alt="Remy Sharp" src={doctor} />}
                        name={<p>Dr. Jordan Peterson</p>}
                    />
                </Grid>

                <Grid item xs={12} sm={6} md={4}>
                    <UpcomingEvents
                        image={metoo}
                        title={<p>Survivors of sexual abuse and their supporters</p>}  
                        date={<p>Sep 14, 2021</p>}
                        time={<p>2.15 PM</p>}
                        avatar={<Avatar alt="Travis Howard" src={unsplashman} />}
                        name={<parseInt>Pst. Jane Doe</parseInt>}
                    />
                </Grid>

                <Grid item xs={12} sm={6} md={4}>
                    <UpcomingEvents
                        image={sad}
                        title={<p>Grieving and loss with the pandemic in mind</p>}  
                        date={<p>Sep 23, 2021</p>}
                        time={<p>4.00 PM</p>}
                        avatar={<Avatar alt="Cindy Baker" src={hope} />}
                        name={<p>Hope Organization</p>}
                    />
                </Grid>
            </Grid>

            <Footer />

        </Container>
    )
}

export default Home;
