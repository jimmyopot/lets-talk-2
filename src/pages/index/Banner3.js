import React from 'react'
import { makeStyles } from '@mui/styles';
import { Card, CardContent, CardMedia } from '@mui/material';
import teach from '../../images/teach.jpg'
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        marginTop: "60px",
        marginBottom: "30px",
        height: "180px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            marginTop: "10px",
        },
    },
    cardleft: {
        width: "230px",
        padding: "5px",
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
    cardmedia: {
        marginTop: "15px",
    },
    cardmid: {
        width: "500px",
        [theme.breakpoints.down("sm")]: {
            width: "380px",
            marginBottom: "10px",
            height: "130px",
            paddingTop: "20px",
        },
    },
    header: {
        color: theme.palette.error.main,
        marginTop: "15px",
        marginLeft: "30px",
        [theme.breakpoints.down("sm")]: {
            color: "skyblue",
            fontSize: "16px",
            marginTop: "-10px",
            marginLeft: "0px",
        },
    },
    bodytext: {
        color: "gray", 
        fontSize: "13px", 
        marginLeft: "30px",
        [theme.breakpoints.down("sm")]: {
            color: "gray",
            fontSize: "12px",
            marginTop: "10px",
            marginLeft: "0px",
            marginBottom: "10px",
        },
    },
    cardright: {
        width: "330px",
        [theme.breakpoints.down("sm")]: {
            width: "230px",
            marginBottom: "10px",
            height: "150px",
            // paddingTop: "20px",
        },
    },
    buttoncard: {
        backgroundColor: "gray", 
        marginTop: "60px",
        marginLeft: "70px",
        borderRadius: "25px",
        textTransform: "capitalize",
        color: "white",
        padding: "10px",
        border: "none",
        [theme.breakpoints.down("sm")]: {
            color: "white",
            marginTop: "60px",
            width: "100px",
            marginLeft: "20px",
            fontSize: "13px",
            padding: "7px",
        },
    },
    signinlink: {
        color: theme.palette.secondary.main,
        [theme.breakpoints.down("sm")]: {
            color: theme.palette.error.main,
        },
    },
}))

function Banner3() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.cardleft} style={{backgroundColor: "whitesmoke"}}>
                <CardMedia
                    component="img"
                    height="140"
                    image={teach}
                    alt="counsellor"
                    className={classes.cardmedia}
                />
            </Card>
    
            <Card className={classes.cardmid} style={{backgroundColor: "whitesmoke"}}>
                <CardContent>
                    <h1 className={classes.header}>Become a counsellor</h1>
                    <div style={{marginTop: "10px"}}>
                        <h5 className={classes.bodytext}>
                        Therapist | Counsellor | Religious Leader | Motivational Speaker |<br />
                        Psychologist | Special groups / Organizations
                        </h5>
                    </div>
                </CardContent>
            </Card>

            <Card className={classes.cardright} style={{backgroundColor: "whitesmoke"}}>
                <button 
                    variant="contained" 
                    className={classes.buttoncard}
                    // style={{
                    //     backgroundColor: "gray", 
                    //     marginTop: "60px",
                    //     marginLeft: "70px",
                    //     borderRadius: "25px",
                    //     textTransform: "capitalize",
                    //     color: "white",
                    // }}
                >
                    <Link to="/signup" style={{textDecoration: "none"}}>
                        <p className={classes.signinlink}>Become a counsellor</p>
                    </Link>
                </button>
            </Card>
            </div>
    )
}

export default Banner3
