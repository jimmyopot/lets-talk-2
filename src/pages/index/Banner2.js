import React from 'react'
import { makeStyles } from '@mui/styles';
import { Card, CardContent, CardMedia } from '@mui/material';
import thinking from '../../images/thinking.jpg'
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        marginTop: "60px",
        marginBottom: "30px",
        height: "180px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            marginTop: "10px",
        },
    },
    cardleft: {
        width: "230px",
        padding: "5px",
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
    cardmedia: {
        marginTop: "15px",
    },
    cardmid: {
        width: "500px",
        [theme.breakpoints.down("sm")]: {
            width: "380px",
            marginBottom: "10px",
            height: "100px",
            paddingTop: "20px",
        },
    },
    header: {
        color: theme.palette.error.main,
        marginTop: "15px",
        marginLeft: "30px",
        [theme.breakpoints.down("sm")]: {
            color: "skyblue",
            fontSize: "16px",
            marginTop: "-10px",
            marginLeft: "0px",
        },
    },
    bodytext: {
        color: theme.palette.info.main, 
        fontSize: "13px", 
        marginLeft: "30px",
        [theme.breakpoints.down("sm")]: {
            color: "gray",
            fontSize: "12px",
            marginTop: "10px",
            marginLeft: "0px",
            marginBottom: "10px",
        },
    },
    cardright: {
        width: "330px",
        [theme.breakpoints.down("sm")]: {
            width: "230px",
            marginBottom: "10px",
            height: "120px",
            // paddingTop: "20px",
        },
    },
    button: {
        backgroundColor: "gray", 
        marginTop: "60px",
        marginLeft: "100px",
        borderRadius: "25px",
        textTransform: "capitalize",
        color: "white",
        padding: "10px",
        paddingLeft: "30px",
        paddingRight: "30px",
        cursor: "pointer",
        border: "none",
        [theme.breakpoints.down("sm")]: {
            marginTop: "40px",
            width: "80px",
            marginLeft: "30px",
            fontSize: "13px",
            padding: "7px",
            paddingLeft: "10px",
            paddingRight: "10px",
        },
    },
    signinlink: {
        color: theme.palette.secondary.main,
        [theme.breakpoints.down("sm")]: {
        color: theme.palette.error.main,
    },
    },
}))

function Banner2() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.cardleft} style={{backgroundColor: "whitesmoke"}}>
                <CardMedia
                    component="img"
                    height="140"
                    image={thinking}
                    alt="counsellor"
                    className={classes.cardmedia}
                />
            </Card>

            <Card className={classes.cardmid} style={{backgroundColor: "whitesmoke"}}>
                <CardContent>
                    <h1 className={classes.header}>Not sure where to start?</h1>
                    <div style={{marginTop: "10px"}}>
                        <h5 className={classes.bodytext}>
                            If you need any help navigating our website, view our<br />
                            <Link to="/about" style={{color: "blue"}}>Help section</Link>
                        </h5>
                    </div>
                </CardContent>
            </Card>

            <Card className={classes.cardright} style={{backgroundColor: "whitesmoke"}}>
                <button 
                    variant="contained" 
                    className={classes.button}
                >
                    <Link to="/signup" style={{textDecoration: "none"}}>
                        <p className={classes.signinlink}>Sign in</p>
                    </Link>
                </button>
                
            </Card>
        </div>
    )
}

export default Banner2
