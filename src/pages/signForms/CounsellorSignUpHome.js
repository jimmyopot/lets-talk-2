import React from 'react';
import { makeStyles } from '@mui/styles';
import { Grid } from '@mui/material';
import CounsellorSignUpLeft from './CounsellorSignUpLeft';
import CounsellorSignUp from './CounsellorSignUp';

const useStyles = makeStyles((theme) => ({
    right: {
       
    }
}))

function CounsellorSignUpHome() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item sm={3} xs={2}>
                    <CounsellorSignUpLeft />
                </Grid>
                <Grid item sm={9} xs={10}>
                    <CounsellorSignUp />
                </Grid>
            </Grid>
        </div>
    )
}

export default CounsellorSignUpHome;
