import React from 'react';
import { makeStyles } from '@mui/styles';
import { Card, CardMedia } from '@mui/material';
import dunk from '../../images/dunk.jpg';

const useStyles = makeStyles((theme) => ({
    card: {
        height: "90vh",
    },
    cardmedia: {
        minWidth: 50,
        minHeight: 100,
        paddingTop: "130px",
    },
}))

function LoginLeft() {
    const classes = useStyles();

    return (
        <div>
            <Card className={classes.card} style={{ backgroundColor: "#9CDAEF" }}>
                <CardMedia
                    component="img"
                    image={dunk}
                    alt="colorart"
                    className={classes.cardmedia}
                />
            </Card>
        </div>
    )
}

export default LoginLeft
