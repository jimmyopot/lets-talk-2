import React, { useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Grid, TextField, FormGroup, FormControlLabel, Checkbox, Button, Typography  } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import doctor from '../../images/doctor.jpg';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import userActions from '../../redux/actions/userActions';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: "50px",
        padding: "20px",
        height: "450px",
        width: "400px",
        margin: "20px auto",
    },
    emailName: {
        display: "flex",
    },
    
}))

function SignUp(props) {
    const classes = useStyles();

    const [user, setUser] = useState({
        userName: "",
        email: "",
        password: "",
    })

    const handleSubmit = (event) => {
        event.preventDefault();
        const { dispatch } = props;
        if (user.userName && user.email && user.userName && user.password) {
            dispatch(userActions.register(user));
        }
    };

    return (
        <div className={classes.paper}>
            <Grid>
                {/* <Paper elevation={10} className={classes.paper}> */}
                    <Grid align="center">
                        <Avatar
                            alt="Remy Sharp"
                            src={doctor}
                            sx={{ width: 40, height: 40 }}
                        />
                        <h2 style={{ marginBottom: "10px" }}>Counsellors Sign Up</h2>
                    </Grid>
                    <form noValidate onSubmit={handleSubmit}>
                        <div className={classes.emailName}>
                            <TextField 
                                id="outlined-basic" 
                                label="Name" 
                                variant="outlined" 
                                size="small"
                                // fullWidth 
                                required 
                                value={user.userName}
                                onChange={(e) => setUser({ ...user, userName: e.target.value })}
                                style={{marginBottom: "15px", width: "200px", marginRight: "30px"}}
                                color="secondary"
                            />
                            <TextField 
                                id="outlined-basic" 
                                label="Email" 
                                variant="outlined" 
                                size="small"
                                // fullWidth 
                                required 
                                value={user.email}
                                onChange={(e) => setUser({ ...user, email: e.target.value })}
                                style={{marginBottom: "15px", width: "200px"}}
                                color="secondary"
                            />
                        </div>
                        <TextField 
                            id="outlined-basic" 
                            label="Password" 
                            variant="outlined" 
                            type="password" 
                            size="small"
                            fullWidth 
                            required 
                            value={user.password}
                            onChange={(e) => setUser({ ...user, password: e.target.value })}
                            style={{marginBottom: "15px"}}
                            color="secondary"
                        />
                        <TextField 
                            id="outlined-basic" 
                            label="Confirm Password" 
                            variant="outlined" 
                            type="password" 
                            size="small"
                            fullWidth 
                            required 
                            style={{marginBottom: "15px"}}
                            color="secondary"
                        />
                        <FormGroup>
                            <FormControlLabel control={<Checkbox style={{color: "blue"}} />} 
                            label="I accept the terms and conditions" />
                        </FormGroup>
                        <Button 
                            type="submit" 
                            variant="contained" 
                            fullWidth 
                            style={{backgroundColor: "gray", color: "white", marginBottom: "10px",}}
                        >Sign Up
                        </Button>
                    </form>
                    <Typography>
                        <h5 style={{color: "gray"}}>Do you have an account?
                        </h5><Link to="/counsellorLogin" style={{color: "blue", fontSize: "14px", textDecoration:"none"}}>Login</Link>
                    </Typography>
                {/* </Paper> */}
            </Grid>
        </div>
    )
}

// export default SignUp
function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering,
    };
}
export default connect(mapStateToProps)(SignUp);