import React from 'react';
import { makeStyles } from '@mui/styles';
import { Grid } from '@mui/material';
import LoginLeft from './LoginLeft';
import Login from './Login';


const useStyles = makeStyles((theme) => ({
    right: {
        
    }
}))

function LoginHome() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item sm={3} xs={2}>
                    <LoginLeft />
                </Grid>
                <Grid item sm={9} xs={10}>
                    <Login />
                </Grid>
            </Grid>
        </div>
    )
}

export default LoginHome
