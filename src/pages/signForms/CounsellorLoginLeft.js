import React from 'react';
import { makeStyles } from '@mui/styles';
import { Card, CardMedia } from '@mui/material';
import maskoff from '../../images/maskoff.jpg';

const useStyles = makeStyles((theme) => ({
    card: {
        height: "90vh",
    },
    cardmedia: {
        minWidth: 50,
        minHeight: 100,
        paddingTop: "130px",
    },
}))

function CounsellorLoginLeft() {
    const classes = useStyles();

    return (
        <div>
            <Card className={classes.card} style={{ backgroundColor: "#DEE0DD" }}>
                <CardMedia
                    component="img"
                    image={maskoff}
                    alt="colorart"
                    className={classes.cardmedia}
                />
            </Card>
        </div>
    )
}

export default CounsellorLoginLeft;
