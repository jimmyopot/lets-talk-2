import React from 'react';
import { makeStyles } from '@mui/styles';
import { Card, CardMedia } from '@mui/material';
import people4 from '../../images/people4.jpg';

const useStyles = makeStyles((theme) => ({
    card: {
        height: "90vh",
    },
    cardmedia: {
        minWidth: 50,
        minHeight: 100,
        paddingTop: "130px",
    },
}))

function CounsellorSignUpLeft() {
    const classes = useStyles();

    return (
        <div>
            <Card className={classes.card} style={{ backgroundColor: "#FFFBDE" }}>
                <CardMedia
                    component="img"
                    image={people4}
                    alt="colorart"
                    className={classes.cardmedia}
                />
            </Card>
        </div>
    )
}

export default CounsellorSignUpLeft;
