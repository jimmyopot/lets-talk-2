import React from "react";
import { makeStyles } from "@mui/styles";
import { Grid } from "@mui/material";
import SignUp from "./SignUp";
import SignUpLeft from "./SignUpLeft";

const useStyles = makeStyles((theme) => ({
  right: {
    // [theme.breakpoints.down("sm")]: {
    //     display: "none"
    // },
    // [theme.breakpoints.down("md")]: {
    //     display: "none"
    // }
  },
}));

function SignUpHome() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item sm={3} xs={2}>
          <SignUpLeft />
        </Grid>
        <Grid item sm={9} xs={10}>
          <SignUp />
        </Grid>
      </Grid>
    </div>
  );
}

export default SignUpHome;
