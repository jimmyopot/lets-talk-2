import React from 'react';
import { makeStyles } from '@mui/styles';
import { Grid } from '@mui/material';
import CounsellorLoginLeft from './CounsellorLoginLeft';
import CounsellorLogin from './CounsellorLogin';


const useStyles = makeStyles((theme) => ({
    right: {
        
    }
}))

function CounsellorLoginHome() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item sm={3} xs={2}>
                    <CounsellorLoginLeft />
                </Grid>
                <Grid item sm={9} xs={10}>
                    <CounsellorLogin />
                </Grid>
            </Grid>
        </div>
    )
}

export default CounsellorLoginHome;
