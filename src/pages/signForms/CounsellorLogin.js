import React, { useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Grid, TextField, FormGroup, FormControlLabel, Checkbox, Button, Typography  } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import doctor from '../../images/doctor.jpg';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import userActions from '../../redux/actions/userActions';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: "50px",
        padding: "20px",
        height: "350px",
        width: "300px",
        margin: "20px auto",
    },
}))

function CounsellorLogin( props ) {
    const classes = useStyles();

    const [user, setUser] = useState({
        userName: "",
        password: "",
    })

    const handleSubmit = (event) => {
        event.preventDefault();

        const { dispatch } = props;
        if (user.userName && user.password) {
            dispatch(userActions.login(user));
        }
    };

    return (
        <div className={classes.paper}>
            <Grid> 
                <Grid align="center">
                    <Avatar
                        alt="Remy Sharp"
                        src={doctor}
                        sx={{ width: 40, height: 40 }}
                    />
                    <h2 style={{ marginBottom: "10px" }}>Counsellors Log in</h2>
                </Grid>
                <form noValidate onSubmit={handleSubmit}>
                    <TextField 
                        id="outlined-basic" 
                        label="Username" 
                        variant="outlined" 
                        size="small"
                        fullWidth 
                        required 
                        value={user.userName}
                        onChange={(e) => setUser({ ...user, userName: e.target.value })}
                        style={{marginBottom: "15px"}}
                        color="secondary"
                    />
                    <TextField 
                        id="outlined-basic" 
                        label="Password" 
                        variant="outlined" 
                        size="small"
                        type="password" 
                        fullWidth 
                        required 
                        value={user.password}
                        onChange={(e) => setUser({ ...user, password: e.target.value })}
                        style={{marginBottom: "15px"}}
                        color="secondary"
                    />
                    <FormGroup>
                        <FormControlLabel control={<Checkbox style={{color: "blue"}} />} label="Remember me" />
                    </FormGroup>
                    <Button 
                        type="submit" 
                        variant="contained" 
                        fullWidth 
                        style={{backgroundColor: "gray", color: "white", marginBottom: "10px",}}
                    > Login
                    </Button>
                </form>

                <Typography>
                    <Link to="" style={{color: "blue", fontSize: "14px", textDecoration:"none"}}>Forgot Password?</Link>
                </Typography>
                <Typography>
                    <h5 style={{color: "gray"}}>Do you have an account?</h5><Link to="/counsellorHomepage" style={{color: "blue", fontSize: "14px", textDecoration:"none"}}>Fill in counsellor forms to Sign Up</Link>
                </Typography>
            </Grid>
        </div>
    )
}

// export default Login
function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn,
    };
}
export default connect(mapStateToProps)(CounsellorLogin);