import React from "react";
import { makeStyles } from "@mui/styles";
import {
  Grid,
  TextField,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Button,
  Typography,
} from "@mui/material";
import Avatar from "@mui/material/Avatar";
import doctor from "../../images/doctor.jpg";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import userActions from "../../redux/actions/userActions";

import LoginHelper from "./LoginHelper";
import { useToasts } from "react-toast-notifications";
import { useHistory } from "react-router-dom";
import { CircularProgress } from "@mui/material";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: "50px",
    padding: "20px",
    height: "350px",
    width: "300px",
    margin: "20px auto",
  },
}));

const initialFieldValues = {
  username: "",
  email: "",
  password: "",
};
function Login(props) {
  const { loggingIn } = props;
  const classes = useStyles();
  const { addToast } = useToasts();
  let history = useHistory();

  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("userName" in fieldValues)
      temp.username = fieldValues.username ? "" : "This field is required.";
    if ("password" in fieldValues)
      temp.password = fieldValues.password ? "" : "This field is required.";
    if ("email" in fieldValues)
      temp.email = /^$|.+@.+..+/.test(fieldValues.email)
        ? ""
        : "Email is not valid.";
    setErrors({
      ...temp,
    });
    if (fieldValues === values)
      return Object.values(temp).every((x) => x === "");
  };

  const { values, errors, setErrors, handleInputChange, resetForm } =
    LoginHelper(initialFieldValues, validate);

  const handleSubmit = (event) => {
    event.preventDefault();

    const onSuccess = () => {
      resetForm();
      addToast("Successfully logged In.", {
        appearance: "success",
      });
      history.push("/mysessions");
    };
    const onFailure = () => {
      // resetForm();
      addToast(
        "An Error Occured while sending to backend, Please try again later.",
        { appearance: "error" }
      );
    };
    const { dispatch } = props;
    if (validate()) {
      console.log("Form is Valid.");
      dispatch(
        userActions.login(
          values.username,
          values.password,
          onSuccess,
          onFailure
        )
      );
    } else {
      console.log("not valid");
      addToast("Not all form fields are valid, Check and try again.", {
        appearance: "error",
      });
    }
  };

  return (
    <div className={classes.paper}>
      <Grid>
        <Grid align="center">
          <Avatar
            alt="Remy Sharp"
            src={doctor}
            sx={{ width: 40, height: 40 }}
          />
          <h2 style={{ marginBottom: "10px" }}>Log in</h2>
        </Grid>
        <form noValidate onSubmit={handleSubmit}>
          <TextField
            id="outlined-basic"
            label="Username"
            variant="outlined"
            size="small"
            fullWidth
            required
            style={{ marginBottom: "15px" }}
            color="secondary"
            name="username"
            value={values.username}
            onChange={handleInputChange}
            {...(errors.username && {
              error: true,
              helperText: errors.username,
            })}
          />
          <TextField
            id="outlined-basic"
            label="Password"
            variant="outlined"
            size="small"
            type="password"
            fullWidth
            required
            style={{ marginBottom: "15px" }}
            color="secondary"
            name="password"
            value={values.password}
            onChange={handleInputChange}
            {...(errors.password && {
              error: true,
              helperText: errors.password,
            })}
          />
          <FormGroup>
            <FormControlLabel
              control={<Checkbox style={{ color: "blue" }} />}
              label="Remember me"
            />
          </FormGroup>
          <Button
            type="submit"
            variant="contained"
            fullWidth
            style={{
              backgroundColor: "gray",
              color: "white",
              marginBottom: "10px",
            }}
          >
            {loggingIn ? (
              <CircularProgress size={25} style={{ float: "right" }} />
            ) : (
              <span>Sign In</span>
            )}
          </Button>
        </form>

        <Typography>
          <Link
            to=""
            style={{ color: "blue", fontSize: "14px", textDecoration: "none" }}
          >
            Forgot Password?
          </Link>
        </Typography>
        <Typography>
          <h5 style={{ color: "gray" }}>Do you have an account?</h5>
          <Link
            to="/signup"
            style={{ color: "blue", fontSize: "14px", textDecoration: "none" }}
          >
            Sign Up
          </Link>
        </Typography>
      </Grid>
    </div>
  );
}

// export default Login
function mapStateToProps(state) {
  const { loggingIn } = state.authentication;
  return {
    loggingIn,
  };
}
export default connect(mapStateToProps)(Login);
