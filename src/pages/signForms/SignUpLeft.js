import React from 'react';
import { makeStyles } from '@mui/styles';
import { Card, CardMedia } from '@mui/material';
import people2 from '../../images/people2.jpg';

const useStyles = makeStyles((theme) => ({
    card: {
        height: "90vh",
    },
    cardmedia: {
        minWidth: 50,
        minHeight: 100,
        paddingTop: "130px",
    },
}))

function SignUpLeft() {
    const classes = useStyles();

    return (
        <div>
            <Card className={classes.card} style={{ backgroundColor: "#7D90EF" }}>
                <CardMedia
                    component="img"
                    image={people2}
                    alt="colorart"
                    className={classes.cardmedia}
                />
            </Card>
        </div>
    )
}

export default SignUpLeft
