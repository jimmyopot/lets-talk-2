import React from 'react'
import { makeStyles } from '@mui/styles';
import { Card, CardContent, CardMedia } from '@mui/material';
import StarIcon from '@mui/icons-material/Star';
import FlagIcon from '@mui/icons-material/Flag';
import TodayIcon from '@mui/icons-material/Today';
import LocalAtmIcon from '@mui/icons-material/LocalAtm';
import BookmarkRemoveIcon from '@mui/icons-material/BookmarkRemove';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
        display: "flex",
        width: "750px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
        },
    },
    cardleft: {
        display: "flex",
        width: "300px",
        height: "200px",
        marginLeft: "40px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "420px",
            width: "300px",
            height: "120px",
            border: "none",
            boxShadow: "none",
        },
    },
    image: {
        marginTop: "10px",
        padding: "10px",
        height: "150px",
        borderRadius: "100px",
        [theme.breakpoints.down("sm")]: {
            paddingTop: "0px",
            padding: "100px",
            width: "100px",
            height: "100px",
        },
    },
    cardmid: {
        width: "800px",
        height: "200px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "385px",
            marginTop: "3px",
            width: "350px",
        },
    },
    heading: {
        display: "flex",
        fontSize: "5px",
        color: "black",
        fontWeight: "bold",
    },
    bodytext: {
        color: "gray",
    },
    profilereport: {
        display: "flex",
        marginTop: "10px",
        marginBottom: "10px",
        marginLeft: "15px",
    },
    cardright: {
        width: "400px",
        height: "200px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "385px",
            marginTop: "3px",
            width: "350px",
            height: "150px",
        },
    },
    tagContainer: {
        display: "flex",
        marginLeft: "15px",
        marginTop: "10px",
    },
    tag: {
        marginRight: "20px",
        backgroundColor: "lightgray",
        padding: "5px",
        borderRadius: "10px",
    },

    iconContainer: {
        textAlign: "center",
        marginTop: "20px",
    },
    iconpad: {
        display: "flex", 
        backgroundColor: "lightgray", 
        borderRadius: "20px",
        alignItems: "center", 
        padding: "5px", 
        marginBottom: "10px",
        [theme.breakpoints.down("sm")]: {
            width: "150px",
            marginLeft: "70px",
            marginTop: "-20px",
            marginBottom: "30px",
        },
    },
    icon: {
        [theme.breakpoints.down("sm")]: {
            color: "orange",
        },
    },
}))

function FavoriteCounsellors( { title, image, text, viewProfile, report, tags, tag1, tag2, tag3, icon1, icon2, icon3 } ) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.cardleft} style={{backgroundColor: "white"}}>
                <CardMedia
                    component="img"
                    // height="140"
                    image={image}
                    alt="doc"
                    className={classes.image}
                />
            </Card>

            <Card className={classes.cardmid}>            
                <CardContent className={classes.cardcontent}>
                    <div className={classes.heading}>
                        <p style={{ fontSize: "13px", marginLeft: "15px" }}>{title}</p>
                        <StarIcon style={{fontSize: "13px", marginLeft: "5px", marginTop: "3px", color: "blue"}} />
                    </div>
                    <div component="div" className={classes.bodytext} style={{ fontSize: "16px", marginTop: "5px", marginLeft: "15px" }}>
                        {text}
                    </div>
                    <div className={classes.profilereport}>
                        <p style={{ fontSize: "13px", color: "black"}} className={classes.viewprofile}>{viewProfile}</p>
                        <FlagIcon style={{ fontSize: "15px", color: "green", marginLeft: "180px", marginRight: "5px"}} />
                        <p style={{ fontSize: "13px", color: "black"}} className={classes.report}>{report}</p>
                    </div>
                    <div>
                        <p style={{ color: "black", marginLeft: "15px"}}>{tags}</p>
                    </div>
                    <div className={classes.tagContainer}>
                        <p style={{ fontSize: "16px", color: "gray"}} className={classes.tag}>{tag1}</p>
                        <p style={{ fontSize: "16px", color: "gray"}} className={classes.tag}>{tag2}</p>
                        <p style={{ fontSize: "16px", color: "gray"}} className={classes.tag}>{tag3}</p>
                    </div>
                </CardContent>
            </Card>

            <Card className={classes.cardright} style={{backgroundColor: "white"}}>
                <CardContent>
                    <div className={classes.iconContainer}>
                        <div className={classes.iconpad}>
                            <TodayIcon style={{ marginLeft: "20px"}} className={classes.icon} />
                            <p style={{ fontSize: "13px", color: "gray", marginLeft: "10px"}}>{icon1}</p>
                        </div>
                        <div className={classes.iconpad}>
                            <LocalAtmIcon style={{ marginLeft: "20px"}} className={classes.icon} />
                            <p style={{ fontSize: "13px", color: "gray", marginLeft: "10px"}}>{icon2}</p>
                        </div>
                        <div className={classes.iconpad}>
                            <BookmarkRemoveIcon style={{ marginLeft: "20px"}} className={classes.icon} />
                            <p style={{ fontSize: "13px", color: "gray", marginLeft: "10px"}}>{icon3}</p>
                        </div>
                        
                    </div>
                </CardContent>
            </Card>
            
        </div>
    )
}

export default FavoriteCounsellors;
