import React from 'react'
import { makeStyles } from '@mui/styles';
import { Avatar, Card, CardContent } from '@mui/material';
import StarIcon from '@mui/icons-material/Star';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
        display: "flex",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
        },
    },
    card: {
        display: "flex",
        width: "500px",
        height: "80px",
        marginLeft: "40px",
        [theme.breakpoints.down("sm")]: {
            width: "250px",
            marginLeft: "-10px",
            marginBottom: "3px",
        },
    },
    image: {
        marginTop: "10px",
        marginLeft: "10px",
    },
    content: {
        display: "flex",
        justifyContent: "space-between",
        marginTop: "10px",
    },
    heading: {
        display: "flex",
        fontSize: "5px",
        color: "black",
        fontWeight: "bold",
    },
    icons: {
        display: "flex",
        marginTop: "15px",
        marginRight: "10px",
        [theme.breakpoints.down("sm")]: {
            marginLeft: "20px",
            marginTop: "0px",
            color: "orange",
        },
    },
    iconLabel: {
        marginRight: "50px",
    },
    cardright: {
        width: "200px",
        height: "80px",
        [theme.breakpoints.down("sm")]: {
            width: "250px",
            height: "50px",
            marginLeft: "-10px",
            marginBottom: "3px",
        },
    },
}))

function CurrentSessions( { title, image, icon1, icon2, icon3 } ) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.card} style={{backgroundColor: "whitesmoke"}}>
                
                <Avatar
                    alt="Remy Sharp"
                    src={image}
                    style={{ width: 60, height: 60 }}
                    className={classes.image}

                />
                
                <CardContent className={classes.content}>
                    <div className={classes.heading}>
                        <StarIcon style={{fontSize: "13px", marginRight: "5px", marginTop: "3px", color: "gold"}} />
                        <p style={{ fontSize: "13px" }}>{title}</p>
                    </div>
                </CardContent>
            </Card>

            <Card className={classes.cardright} style={{backgroundColor: "whitesmoke"}}>      
                <CardContent className={classes.iconContainer}>
                    <div className={classes.icons}>
                        <p className={classes.iconLabel}>{icon1}</p>
                        <p className={classes.iconLabel}>{icon2}</p>
                        <p className={classes.iconLabel}>{icon3}</p>
                    </div>                
                </CardContent>
            </Card>
            
        </div>
    )
}

export default CurrentSessions;
