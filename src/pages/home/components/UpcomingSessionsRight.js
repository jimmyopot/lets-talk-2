import React from 'react'
import { makeStyles } from '@mui/styles';
import { Avatar, Card, CardContent } from '@mui/material';
import StarIcon from '@mui/icons-material/Star';
import TimelapseIcon from '@mui/icons-material/Timelapse';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
        // marginBottom: "30px",
        display: "flex",
    },
    card: {
        display: "flex",
        width: "300px",
        height: "100px",
        marginLeft: "10px",
    },
    image: {
        marginTop: "20px",
        marginLeft: "10px",
    },
    heading: {
        display: "flex",
        fontSize: "5px",
        color: "black",
        fontWeight: "bold",
    },
    bodytext: {
        color: "gray",
    },
    datetime: {
        display: "flex",
        marginTop: "15px",
        marginLeft: "15px",
    },
    time: {
        marginLeft: "40px",
    },
}))

function UpcomingSessionsRight( { title, image, text, date, time } ) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.card} style={{backgroundColor: "whitesmoke"}}>
                
                <Avatar
                    alt="Remy Sharp"
                    src={image}
                    style={{ width: 60, height: 60 }}
                    className={classes.image}

                />
                
                <CardContent>
                    <div className={classes.heading}>
                        <StarIcon style={{fontSize: "13px", marginRight: "5px", marginTop: "3px", color: "blue"}} />
                        <p style={{ fontSize: "13px" }}>{title}</p>
                    </div>
                    <div component="div" className={classes.bodytext} style={{ fontSize: "13px", marginTop: "5px", marginLeft: "15px" }}>
                        {text}
                    </div>
                    <div className={classes.datetime}>
                        <TimelapseIcon style={{fontSize: "16px", marginRight: "5px"}} />
                        <p style={{ fontSize: "13px", color: "gray"}} className={classes.date}>{date}</p>
                        <p style={{ fontSize: "13px", color: "gray"}} className={classes.time}>{time}</p>
                    </div>
                </CardContent>
            </Card>
            
        </div>
    )
}

export default UpcomingSessionsRight;
