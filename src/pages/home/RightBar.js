import React from 'react';
import { makeStyles } from '@mui/styles';
import Notifications from './components/Notifications';
import { Grid } from '@mui/material';
import MessageIcon from '@mui/icons-material/Message';
import UpcomingSessionsRight from './components/UpcomingSessionsRight';

const useStyles = makeStyles((theme) => ({
    heading: {
        marginTop: "40px",
        marginLeft: "10px",
    },
}))

function RightBar() {
    const classes = useStyles();

    return (
        <div>
            <div className={classes.heading}>
                <h1 style={{fontSize: "15px", fontWeight: "700"}}>My Activity Notifications</h1>
            </div>
            
            <Grid container spacing={1} gridcontainer>
                <Grid item xs={12} sm={6} md={6}>
                    <Notifications
                        title={<h3>No Answers</h3>}
                        text={<h3>Ask Question</h3>}
                        icon={<MessageIcon />}
                        
                    />
                </Grid>
            </Grid>
            
            <div className={classes.heading}>
                <h1 style={{fontSize: "15px", fontWeight: "700"}}>Upcoming Sessions</h1>
            </div>

            <Grid container spacing={1}>
                <Grid item xs={12} sm={6} md={12}>
                    <UpcomingSessionsRight
                        title={<h3>Dr. Jordan Peterson</h3>}
                        image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                        text={<h4>Appointment</h4>}
                        date={<h5>Oct 1, 2021</h5>}
                        time={<h5>1.30pm</h5>}
                    />
                </Grid>
            </Grid>
        </div>
    )
}

export default RightBar
