import React from 'react';
import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import LeftBar from './LeftBar';
import Feed from './Feed';
import RightBar from './RightBar';

const useStyles = makeStyles((theme) => ({
    right: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
        [theme.breakpoints.down("md")]: {
            display: "none"
        }
    }
}))

function MySessions() {
    const classes = useStyles();
    return (
        <div>
            <Grid container>
                <Grid item sm={2} xs={2}>
                    <LeftBar />
                </Grid>
                <Grid item sm={7} xs={10}>
                    <Feed />
                </Grid>
                <Grid item sm={3} className={classes.right}>
                    <RightBar />
                </Grid>
            </Grid>
        </div>
    )
}

export default MySessions;
