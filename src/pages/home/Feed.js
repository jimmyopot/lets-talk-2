import React from 'react'
import { makeStyles } from '@mui/styles';
import { Grid, InputBase } from '@mui/material';
import { Search } from '@mui/icons-material';
import UpcomingSessions from './components/UpcomingSessions';
import CurrentSessions from './components/CurrentSessions';
import TodayIcon from '@mui/icons-material/Today';
import LocalAtmIcon from '@mui/icons-material/LocalAtm';
import BookmarkRemoveIcon from '@mui/icons-material/BookmarkRemove';
import FavoriteCounsellors from './components/FavoriteCounsellors';

const useStyles = makeStyles((theme) => ({
    search: {
        display: "flex",
        marginTop: "30px",
        marginLeft: "10px",
        borderRadius: "25px",
    },

    searchInput: {
        backgroundColor: "gray",
        width: "720px",
        height: "40px",
        padding:" 15px",
        border:" none",
        [theme.breakpoints.down("sm")]: {
            width: "300px",
        },
    },

    searchIcon: {
        backgroundColor: "gray",
        width: "100px",
        height: "20px",
        padding:" 8px",
        border:" none",
        color: "white",
    },
    heading: {
        color: "black",
        marginTop: "20px",
        marginLeft: "10px",
    },
}))

function Feed() {
    const classes = useStyles();

    return (
        <>
        <div className={classes.search}>
            <InputBase placeholder="Find the right counsellor for you" className={classes.searchInput} style={{color: "white"}} />
            <Search className={classes.searchIcon} />
        </div>
        <div className={classes.heading}>
            <h1 style={{fontSize: "15px", fontWeight: "700"}}>Upcoming Sessions</h1>
        </div>

        <Grid container spacing={1} className={classes.gridcontainer1}>
            <Grid item xs={12} sm={12} md={6}>
                <UpcomingSessions
                    title={<h3>Dr. Jordan Peterson</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h4>Appointment</h4>}
                    date={<h5>Oct 1, 2021</h5>}
                    time={<h5>1.30pm</h5>}
                />
            </Grid>

            <Grid item xs={12} sm={12} md={6}>
                <UpcomingSessions
                    title={<h3>Dr. Jordan Peterson</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h4>Appointment</h4>}
                    date={<h5>Sep 16, 2021</h5>}
                    time={<h5>11.30am</h5>}
                />
            </Grid>
        </Grid>

        <div className={classes.heading}>
            <h1 style={{fontSize: "15px", fontWeight: "700"}}>Current Sessions</h1>
        </div>

        <Grid container spacing={1} className={classes.gridcontainer2}>
            <Grid item xs={12} sm={12} md={12}>
                <CurrentSessions
                    title={<h3>Pst. Cathy Kiuna</h3>}
                    image="https://www.helpguide.org/wp-content/uploads/man-at-worktable-laptop-looking-stressed.jpg"
                    icon1={<TodayIcon />}
                    icon2={<LocalAtmIcon />}
                    icon3={<BookmarkRemoveIcon />}
                />
            </Grid>
        </Grid>
        <Grid container spacing={1} className={classes.gridcontainer2}>
            <Grid item xs={12} sm={12} md={12}>
                <CurrentSessions
                    title={<h3>Dr. Mazwaizi Tuko</h3>}
                    image="https://www.helpguide.org/wp-content/uploads/man-at-worktable-laptop-looking-stressed.jpg"
                    icon1={<TodayIcon />}
                    icon2={<LocalAtmIcon />}
                    icon3={<BookmarkRemoveIcon />}
                />
            </Grid>
        </Grid>

        <div className={classes.heading}>
            <h1 style={{fontSize: "15px", fontWeight: "700"}}>Favorite Counsellors</h1>
        </div>

        <Grid container spacing={1} className={classes.gridcontainer3}>
            <Grid item xs={12} sm={12} md={12}>
                <FavoriteCounsellors
                    title={<h3>Dr. Jordan Makulutu</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h6>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque. 
                    </h6>}
                    viewProfile={<h5>View Profile</h5>}
                    report={<h5>Report</h5>}
                    tags={<h5>Tags:</h5>}
                    tag1={<h6>Abuse</h6>}
                    tag2={<h6>Emotional abuse</h6>}
                    tag3={<h6>Children</h6>}
                    icon1={<h4>Book session</h4>}
                    icon2={<h4>Tip</h4>}
                    icon3={<h4>Remove profile</h4>}
                />
            </Grid>
        <Grid />
        <Grid container spacing={1} className={classes.gridcontainer3}></Grid>
            <Grid item xs={12} sm={12} md={12}>
                <FavoriteCounsellors
                    title={<h3>Pst. Duke Masinde</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h6>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque.
                    </h6>}
                    viewProfile={<h5>View Profile</h5>}
                    report={<h5>Report</h5>}
                    tags={<h5>Tags:</h5>}
                    tag1={<h6>Abuse</h6>}
                    tag2={<h6>Emotional abuse</h6>}
                    tag3={<h6>Adults</h6>}
                    icon1={<h4>Book session</h4>}
                    icon2={<h4>Tip</h4>}
                    icon3={<h4>Remove profile</h4>}
                />
            </Grid>
        </Grid>

        </>
    )
}

export default Feed
