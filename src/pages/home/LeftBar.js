import { Container } from '@mui/material';
import React from 'react';
import { makeStyles } from '@mui/styles';
import { Link } from 'react-router-dom';
import LiveHelpIcon from '@mui/icons-material/LiveHelp';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import ContactPhoneIcon from '@mui/icons-material/ContactPhone';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "whitesmoke",
        marginTop: "5px",
        position: "sticky",
        top: 0,
        height: "100vh",
    },
    linkActivity: {
        paddingTop: "30px",
        paddingBottom: "10px",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.down("sm")]: {
            display: "block",
            fontSize: "15px",
            marginLeft: "-10px",
            color: "gray",
        },
        [theme.breakpoints.up("sm")]: {
            color: "black",
            fontSize: "18px", 
            textDecoration: "none", 
            fontWeight: "700",
        }
    },
    link: {
        paddingTop: "50px",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.down("sm")]: {
            display: "block",
            marginLeft: "20px",
        }
    },
    text: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
    },
    textZero: {
        marginLeft: "60px",
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
        [theme.breakpoints.down("md")]: {
            display: "none"
        },
    },
}))

function LeftBar() {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            <div className={classes.linkActivity}>
                <h3>Activity</h3>
            </div>
            <div className={classes.linkQuestion}>
                <div className={classes.link}>
                    <LiveHelpIcon style={{marginLeft: "-10px", marginRight: "5px", color: "skyblue"}} className={classes.linkIcon}/>
                    <Link to="/" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>My Questions</Link>
                    <p className={classes.textZero}>0</p>
                </div>
               
            </div>
            <div className={classes.link}>
                <AttachFileIcon style={{marginLeft: "-10px", marginRight: "5px", color: "skyblue"}} className={classes.linkIcon} />
                <Link to="/" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>My Sessions</Link>
            </div>
            <div className={classes.link}>
                <ContactPhoneIcon style={{marginLeft: "-10px", marginRight: "5px", color: "skyblue"}} className={classes.linkIcon} />
                <Link to="/" style={{color: "gray", fontSize: "13px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>My Counsellors</Link>
            </div>
            <div className={classes.link}>
                <MeetingRoomIcon style={{marginLeft: "-10px", marginRight: "5px", color: "skyblue"}} className={classes.linkIcon} />
                <Link to="/" style={{color: "gray", fontSize: "15px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>Room</Link>
            </div>
        </Container>
    )
}

export default LeftBar
