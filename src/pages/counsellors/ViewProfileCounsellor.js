import React from 'react';
import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import ViewProfileCounsellorLeft from './ViewProfileCounsellorLeft';
import ViewProfileCounsellorFeed from './ViewProfileCounsellorFeed';
import ViewProfileCounsellorRight from './ViewProfileCounsellorRight';

const useStyles = makeStyles((theme) => ({
    right: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
        [theme.breakpoints.down("md")]: {
            display: "none"
        }
    }
}))

function ViewProfileCounsellor() {
    const classes = useStyles();
    
    return (
        <div>
             <Grid container>
                <Grid item sm={2} xs={2}>
                    <ViewProfileCounsellorLeft />
                </Grid>
                <Grid item sm={7} xs={10}>
                    <ViewProfileCounsellorFeed />
                </Grid>
                <Grid item sm={3} className={classes.right}>
                    <ViewProfileCounsellorRight />
                </Grid>
            </Grid>
        </div>
    )
}

export default ViewProfileCounsellor;
