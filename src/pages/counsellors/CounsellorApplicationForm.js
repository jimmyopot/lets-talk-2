import React, { useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Button, Step, StepLabel, Stepper } from '@mui/material';
import FormStep1 from './FormStep1';
import FormStep2 from './FormStep2';
import FormStep3 from './FormStep3';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "20px 0px",
        "& .MuiStepIcon-text.css-opt7yd-MuiStepIcon-text": {
            color: "red"
        }
    },
}))

function CounsellorApplicationForm() {
    const classes = useStyles();

    // Hooks
    const [activeStep, setActiveStep] = useState(0);

    const handleNext = () => {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    }

    const getSteps = () => {
        return ["PERSONAL INFORMATION", "PROFESSIONAL INFORMATION", "UPLOADS"]
    };

    const steps = getSteps();

    const getStepsContent = (stepIndex) => {
        switch(stepIndex) {
            case 0:
                return <FormStep1 />;
            case 1:
                return <FormStep2 />;
            case 2:
                return <FormStep3 />;
            default:
                return "Unknown Step"
        }
    }

    return (
        <div>
            <Stepper
                alternativeLabel 
                activeStep={activeStep}
                className={classes.root}
            >
                {steps.map((label) => (
                    <Step key={label}>
                        <StepLabel>
                            {label}
                        </StepLabel>
                    </Step>
                ))}             
            </Stepper>
            <>
                {activeStep === steps.length ? "The Steps are Completed" : (
                    <>
                    {getStepsContent(activeStep)}
                    <Button 
                        variant="contained" 
                        color="secondary" 
                        onClick={handleNext} 
                        style={{color: "white", marginLeft: "250px", marginTop: "0px"}}
                    >
                        {activeStep === steps.length ? "Finish" : "Next"}
                    </Button>
                    </>
                )}
            </>
        </div>
    )
}

export default CounsellorApplicationForm
