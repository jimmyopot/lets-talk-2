import React from 'react';
import { makeStyles } from '@mui/styles';
import { Grid } from '@mui/material';
import AdminLeftBar from './components/AdminLeftBar';
import AdminRightBar from './components/AdminRightBar';

const useStyles = makeStyles((theme) => ({

}))

function AdminPanel() {
    const classes = useStyles();

    return (
        <div>
            <div className={classes.root}>
                <Grid container>
                    <Grid item sm={2} xs={2}>
                        <AdminLeftBar />
                    </Grid>
                    <Grid item sm={10} xs={10}>
                        <AdminRightBar />
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

export default AdminPanel
