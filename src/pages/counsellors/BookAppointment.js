import React from 'react';
import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import BookAppointmentLeft from './BookAppointmentLeft';
import BookAppointmentFeed from './BookAppointmentFeed';
import BookAppointmentRight from './BookAppointmentRight';

const useStyles = makeStyles((theme) => ({
    right: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
        [theme.breakpoints.down("md")]: {
            display: "none"
        }
    }
}))

function BookAppointment() {
    const classes = useStyles();
    
    return (
        <div>
            <Grid container>
                <Grid item sm={1} xs={2}>
                    
                </Grid>
                <Grid item sm={3} xs={2}>
                    <BookAppointmentLeft />
                </Grid>
                <Grid item sm={4} xs={10}>
                    <BookAppointmentFeed />
                </Grid>
                <Grid item sm={3} className={classes.right}>
                    <BookAppointmentRight />
                </Grid>
                <Grid item sm={1} xs={2}>
                    
                </Grid>
            </Grid>
        </div>
    )
}

export default BookAppointment;
