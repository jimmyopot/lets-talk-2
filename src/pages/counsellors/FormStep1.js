import React, {useState} from 'react';
import { makeStyles } from '@mui/styles';
import {FormControl, InputLabel, MenuItem, Select, TextField, FormHelperText} from '@mui/material';
import CountrySelect from './components/SelectCountryDropdown';
import BasicDatePicker from './components/AgeDatePicker';

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: "-170px",
    },
    form: {
        display: "flex",
        flexDirection: "column",
    },
    formControl: {
        width: "200px",
    },
    names: {
        display: "flex",

    },
    email: {
        display: "flex",

    },
    genderLanguage: {
        display: "flex",

    },
}))

function FormStep1() {
    const classes = useStyles();
    const [gender, setGender] = useState('');
    const [language, setLanguage] = useState('');

    const handleChangeGender = (event) => {
        setGender(event.target.value);
    };

    const handleChangeLanguage = (event) => {
        setLanguage(event.target.value);
    };

    return (
        <div className={classes.root}>
            <form className={classes.form}>
                <div className={classes.names}>
                    <TextField 
                        id="outlined-basic" 
                        label="First Name" 
                        variant="outlined"
                        color="error" 
                        style={{width: "180px", marginBottom: "20px", marginLeft: "180px"}}
                    />
                    <TextField 
                        id="outlined-basic" 
                        label="Middle Name" 
                        variant="outlined"
                        color="error" 
                        style={{width: "180px", marginBottom: "20px", marginLeft: "30px"}}
                    />
                    <TextField 
                        id="outlined-basic" 
                        label="Last Name" 
                        variant="outlined"
                        color="error" 
                        style={{width: "180px", marginBottom: "20px", marginLeft: "30px"}}
                    />
                </div>
                <div className={classes.email}>
                    <TextField 
                        id="outlined-basic" 
                        label="UserName" 
                        variant="outlined"
                        color="error" 
                        style={{width: "320px", marginBottom: "20px", marginLeft: "180px"}}
                    />
                    <TextField 
                        id="outlined-basic" 
                        label="Email" 
                        variant="outlined"
                        color="error" 
                        style={{width: "320px", marginBottom: "20px", marginLeft: "60px"}}
                    />
                </div>

                {/* dropdowns */}
                
                <BasicDatePicker />
                <div className={classes.genderLanguage}>
                    <FormControl color="error" className={classes.formControl} style={{marginBottom: "30px", marginLeft: "180px", width: "320px",}}>
                        <InputLabel id="demo-simple-select-label" className={classes.inputLabel}>Gender</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={gender}
                            label="Gender"
                            onChange={handleChangeGender}
                        >
                            <MenuItem value={"None"}><em>None</em></MenuItem>
                            <MenuItem value={"male"}>Male</MenuItem>
                            <MenuItem value={"female"}>Female</MenuItem>
                            <MenuItem value={"other"}>Other</MenuItem>
                        </Select>
                        <FormHelperText>Select only one</FormHelperText>
                    </FormControl>

                    <FormControl color="error" className={classes.formControl} style={{marginBottom: "30px", border: "none", marginLeft: "60px", width: "320px",}}>
                        <InputLabel id="demo-simple-select-label">Language</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={language}
                            label="Language"
                            onChange={handleChangeLanguage}
                        >
                            <MenuItem value={"Engilsh"}>Engilsh</MenuItem>
                            <MenuItem value={"Swahili"}>Swahili</MenuItem>
                            <MenuItem value={"French"}>French</MenuItem>
                            <MenuItem value={"German"}>German</MenuItem>
                        </Select>
                        <FormHelperText>Select only one</FormHelperText>
                    </FormControl>
                </div>

                <CountrySelect style={{marginBottom: "30px"}} />

            </form>
        </div>
    )
}

export default FormStep1;

