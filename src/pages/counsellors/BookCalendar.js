import React, { useState } from 'react';
import { makeStyles } from '@mui/styles';
import { Button } from '@mui/material';
import BookCalendarSearch from './BookCalendarSearch';

const useStyles = makeStyles((theme) => ({
    searchButton: {
        fontWeight: 900,
        marginTop: "20px",
        marginLeft: "20px",
    },
}))

function BookCalendar() {
    const classes = useStyles();
    const [showSearch, setShowSearch] = useState(false);

    return (
        <div classname={classes.root}>
            <div>
                <Button 
                    onClick={() => {
                        setShowSearch(!showSearch)
                    }}
                    color="secondary" 
                    variant="outlined" 
                    style={{fontWeight: 600, marginTop: "20px", marginLeft: "300px", marginBottom: "60px"}}>
                    Search Dates
                </Button>
                {showSearch && <BookCalendarSearch /> }
            </div>
        </div>
    )
}

export default BookCalendar
