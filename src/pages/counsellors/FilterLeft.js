import { Container } from '@mui/material';
import React from 'react';
import { makeStyles } from '@mui/styles';
import { Link } from 'react-router-dom';
import FilterTherapySelect from './components/FilterTherapySelect';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "whitesmoke",
        marginTop: "1px",
        position: "sticky",
        top: 0,
        height: "100vh",
    },
    linkActivity: {
        paddingTop: "30px",
        paddingBottom: "10px",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.down("sm")]: {
            display: "none",
            fontSize: "15px",
            marginLeft: "-10px",
            color: "gray",
        },
        [theme.breakpoints.up("sm")]: {
            color: "black",
            fontSize: "18px", 
            textDecoration: "none", 
            fontWeight: "700",
        }
    },
    link: {
        paddingTop: "20px",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.down("sm")]: {
            display: "block",
            marginLeft: "20px",
        }
    },
    link2: {
        paddingTop: "10px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        [theme.breakpoints.down("sm")]: {
            display: "block",
            marginLeft: "20px",
        }
    },
    text: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
    },
    textZero: {
        marginLeft: "60px",
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
        [theme.breakpoints.down("md")]: {
            display: "none"
        },
    },
    selectTherapy: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
        [theme.breakpoints.down("md")]: {
            display: "none"
        },
    },
}))

function FilterLeft() {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            <div className={classes.linkActivity}>
                <h3>Filter</h3>
            </div>
            <div className={classes.linkQuestion}>
                <div className={classes.link}>
                    <Link to="/" style={{color: "gray", fontSize: "14px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>Counsellor type</Link>
                </div>
                <div className={classes.link2}>
                    <Link to="/" style={{color: "gray", marginLeft: "-100px", paddingBottom: "10px", fontSize: "13px", textDecoration: "none", fontWeight: "600"}} className={classes.text}>Medical</Link>
                    <Link to="/" style={{color: "gray", marginLeft: "-90px", paddingBottom: "10px", fontSize: "13px", textDecoration: "none", fontWeight: "600"}} className={classes.text}>Religious</Link>
                    <Link to="/" style={{color: "gray", marginLeft: "-70px", paddingBottom: "10px", fontSize: "13px", textDecoration: "none", fontWeight: "600"}} className={classes.text}>Motivational</Link>
                    <Link to="/" style={{color: "gray", marginLeft: "-30px", fontSize: "13px", textDecoration: "none", fontWeight: "600"}} className={classes.text}>Group/Organization</Link>
                </div>
               
            </div>
            <div className={classes.link}>
                <Link to="/" style={{color: "gray", fontSize: "14px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>Gender</Link>
            </div>
            <div className={classes.link2}>
                <Link to="/" style={{color: "gray", marginLeft: "-100px", paddingBottom: "10px", fontSize: "13px", textDecoration: "none", fontWeight: "600"}} className={classes.text}>Male</Link>
                <Link to="/" style={{color: "gray", marginLeft: "-90px", paddingBottom: "10px", fontSize: "13px", textDecoration: "none", fontWeight: "600"}} className={classes.text}>Female</Link>
            </div>

            <div className={classes.link}>
                <Link to="/" style={{color: "gray", fontSize: "14px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>Type of therapy</Link>
            </div>
            <div className={classes.selectTherapy}>
                <FilterTherapySelect />
            </div>
        </Container>
    )
}

export default FilterLeft;
