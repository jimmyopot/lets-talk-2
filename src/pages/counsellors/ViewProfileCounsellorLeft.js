import { Container } from '@mui/material';
import React from 'react';
import { makeStyles } from '@mui/styles';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "whitesmoke",
        marginTop: "1px",
        position: "sticky",
        top: 0,
        height: "100vh",
    },
    backtosearchMatchContainer: {
        paddingTop: "200px",
        display: "flex",
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
    backtosearch: {
        color: theme.palette.info.main, 
    },
    arrowForward: {
        marginTop: "5px",
        borderRadius: "25px",
        marginRight: "5px",
        backgroundColor: "lightgray",
    },
}))

function ViewProfileCounsellorLeft() {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            <div className={classes.backtosearchMatchContainer}>
                <ArrowBackIcon className={classes.arrowForward} style={{fontSize: "15px"}} />
                <Link to="/counsellorFilterPage" style={{textDecoration: "none"}}>
                    <h4 className={classes.backtosearch}>Back to search</h4>
                </Link>
            </div>
        </Container>
    )
}

export default ViewProfileCounsellorLeft;
