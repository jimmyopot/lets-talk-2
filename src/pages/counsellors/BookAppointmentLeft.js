import React from 'react'
import { makeStyles } from '@mui/styles';
import { Button, Card, CardContent, CardMedia } from '@mui/material';
import StarPurple500Icon from '@mui/icons-material/StarPurple500';
import { Link } from "react-router-dom";
import people from '../../images/unsplashwoman.jpg'

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "white",
        marginTop: "1px",
        position: "sticky",
        top: 0,
        height: "100vh",
    },
    card: {
        display: "flex",
        marginTop: "250px",
        width: "360px",
    },
    cardleft: {
        backgroundColor: "white",
        display: "flex",
        flexDirection: "column",
        width: "130px",
        height: "200px",
        marginLeft: "10px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "420px",
            width: "300px",
            height: "120px",
            border: "none",
            boxShadow: "none",
        },
    },
    image: {
        marginTop: "10px",
        marginLeft: "3px",
        padding: "10px",
        height: "130px",
        borderRadius: "100px",
        [theme.breakpoints.down("sm")]: {
            paddingTop: "0px",
            padding: "100px",
            width: "100px",
            height: "100px",
        },
    },
    cardright: {
        backgroundColor: "whitesmoke",
        height: "200px",
        marginRight: "30px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "385px",
            marginTop: "3px",
            width: "350px",
        },
    },
    bookHeader: {
        fontSize: "16px",
        fontWeight: "700",
        marginBottom: "10px",
        color: theme.palette.info.main,
    },
    nameHeader: {
        fontSize: "16px",
        fontWeight: "600",
        marginBottom: "10px",
        color: theme.palette.info.main,
    },
    tags: {
        fontSize: "13px",
        marginTop: "15px",
        marginBottom: "15px",
        color: theme.palette.info.main,
    },
    childAbuse: {
        display: "flex",
    },
}))

function BookAppointmentLeft() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.card}>
                <Card className={classes.cardleft} style={{backgroundColor: "white"}}>
                    <CardMedia
                        component="img"
                        image={people}
                        alt="doc"
                        style={{ width: 80 }}
                        className={classes.image}
                    />
                    <div className={classes.imageIcons}>
                        <StarPurple500Icon color="secondary" style={{ marginBottom: "15px", marginTop: "-5px", marginLeft: "40px" }} />
                    </div>
                </Card>

                <Card className={classes.cardright}>            
                    <CardContent className={classes.cardcontent}>
                        <div>
                            <p className={classes.bookHeader}>Book Appointment</p>
                        </div>
                        <div>
                            <p className={classes.nameHeader}>Dr.Jordan Peterson</p>
                        </div>
                        
                        <div>
                            <h3 className={classes.tags}>Tags:</h3>
                        </div>
                        <div style={{ marginBottom: "15px" }} className={classes.childAbuse}>
                            <Link to="" style={{ textDecoration: "none" }}>
                                <Button variant="contained" style={{ maxWidth: '25px', maxHeight: '25px', fontSize: "10px", marginRight: "20px" }}>
                                    Abuse
                                </Button>
                            </Link>
                            <Link to="" style={{ textDecoration: "none" }}>
                                <Button variant="contained" style={{ maxHeight: '25px', fontSize: "10px" }}>
                                    Children
                                </Button>
                            </Link>
                        </div>
                        <div>
                            <Link to="" style={{ textDecoration: "none" }}>
                                <Button variant="contained" style={{ maxHeight: '25px', fontSize: "10px" }}>
                                    Emotional abuse
                                </Button>
                            </Link>
                        </div>
                    </CardContent>
                </Card>
            </div>
        </div>
    )
}

export default BookAppointmentLeft;
