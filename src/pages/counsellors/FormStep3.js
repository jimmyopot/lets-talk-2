import React from 'react';
import { makeStyles } from '@mui/styles';
import FileUpload from './components/FileUpload';

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: "150px",
    },
}))

function FormStep3() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <FileUpload />
        </div>
    )
}

export default FormStep3;

