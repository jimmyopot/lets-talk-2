import { Container } from '@mui/material';
import React from 'react';
import { makeStyles } from '@mui/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "whitesmoke",
        marginTop: "0px",
        position: "sticky",
        top: 0,
        height: "100vh",
    },
    linkActivity: {
        paddingTop: "30px",
        paddingBottom: "10px",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.down("sm")]: {
            display: "block",
            fontSize: "15px",
            marginLeft: "-10px",
            color: "gray",
        },
        [theme.breakpoints.up("sm")]: {
            color: "black",
            fontSize: "18px", 
            textDecoration: "none", 
            fontWeight: "700",
        }
    },
    link: {
        paddingTop: "50px",
        display: "flex",
        alignItems: "center",
        [theme.breakpoints.down("sm")]: {
            display: "block",
            marginLeft: "20px",
        }
    },
    text: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
    },
    textZero: {
        marginLeft: "60px",
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
        [theme.breakpoints.down("md")]: {
            display: "none"
        },
    },
}))

function LandingPageLeft() {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            <div className={classes.linkQuestion}>
                <div className={classes.link}>
                    <Link 
                        to="/counsellorDataTable" 
                        style={{color: "gray", fontSize: "15px", textDecoration: "none", fontWeight: "700"}} 
                        className={classes.text}
                    >
                        Dashboard
                    </Link>
                </div>
            </div>
            <div className={classes.link}>
                <Link to="/" style={{color: "gray", fontSize: "15px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>Counsellor Setup</Link>
            </div>
            <div className={classes.link}>
                <Link to="/" style={{color: "gray", fontSize: "15px", textDecoration: "none", fontWeight: "700"}} className={classes.text}>Set up</Link>
            </div>
        </Container>
    )
}

export default LandingPageLeft
