import React, {useState} from 'react';
import { makeStyles } from '@mui/styles';
import {FormControl, InputLabel, MenuItem, Select, FormHelperText, TextField } from '@mui/material';

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: "-170px",
    },
    form: {
        display: "flex",
        flexDirection: "column",
    },
    formControl: {
        width: "200px",
    },
    careerEducation: {
        display: "flex",
        marginBottom: "20px", 
    },
    rates: {
        display: "flex",
        marginBottom: "20px", 
    },
}))

function FormStep2() {
    const classes = useStyles();
    const [education, setEducation] = useState('');
    const [careerSpecialization, setCareerSpecialization] = useState('');
    const [yearsOfExperience, setYearsOfExperience] = useState('');
    const [currentOccupation, setCurrentOccupation] = useState('');


    const handleChangeEducation = (event) => {
        setEducation(event.target.value);
    };

    const handleChangeCareerSpecialization = (event) => {
        setCareerSpecialization(event.target.value);
    };

    const handleChangeYearsOfExperience = (event) => {
        setYearsOfExperience(event.target.value);
    };
    
    const handleChangeCurrentOccupation = (event) => {
        setCurrentOccupation(event.target.value);
    };

    return (
        <div className={classes.root}>
            <form className={classes.form}>
                {/* dropdowns */}
                <div className={classes.careerEducation}>
                    <FormControl color="error" className={classes.formControl} style={{marginLeft: "180px", width: "180px",}}>
                        <InputLabel id="demo-simple-select-label" style={{fontSize: "14px"}}>Education Level</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={education}
                            label="Education Level"
                            onChange={handleChangeEducation}
                        >
                            <MenuItem value={"Primary"}>Primary</MenuItem>
                            <MenuItem value={"Secondary"}>Secondary</MenuItem>
                            <MenuItem value={"Degree"}>Degree</MenuItem>
                            <MenuItem value={"Masters"}>Masters</MenuItem>
                            <MenuItem value={"PHD"}>PHD</MenuItem>
                        </Select>
                        <FormHelperText>Select only one</FormHelperText>
                    </FormControl>

                    <FormControl color="error" className={classes.formControl} style={{marginLeft: "30px", width: "180px",}}>
                        <InputLabel id="demo-simple-select-label" className={classes.inputLabel} style={{fontSize: "14px"}}>Career Specialization</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={careerSpecialization}
                            label="Career Specialization"
                            onChange={handleChangeCareerSpecialization}
                        >
                            <MenuItem value={"Therapist"}>Therapist</MenuItem>
                            <MenuItem value={"Counsellor"}>Counsellor</MenuItem>
                            <MenuItem value={"Psychologist"}>Psychologist</MenuItem>
                            <MenuItem value={"Religious Leader"}>Religious Leader</MenuItem>
                            <MenuItem value={"Motivational Speaker"}>Motivational Speaker</MenuItem>
                        </Select>
                        <FormHelperText>Select only one</FormHelperText>
                    </FormControl>

                    <FormControl color="error" className={classes.formControl} style={{marginLeft: "30px", width: "180px",}}>
                        <InputLabel id="demo-simple-select-label" style={{fontSize: "14px"}}>Years of Experience</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={yearsOfExperience}
                            label="Years of Experience"
                            onChange={handleChangeYearsOfExperience}
                        >
                            <MenuItem value={1}>1</MenuItem>
                            <MenuItem value={2}>2</MenuItem>
                            <MenuItem value={2}>3</MenuItem>
                            <MenuItem value={4}>4</MenuItem>
                            <MenuItem value={5}>5</MenuItem>
                            <MenuItem value={6}>6</MenuItem>
                        </Select>
                        <FormHelperText>Select only one</FormHelperText>
                    </FormControl>
                </div>

                <TextField
                    color="error"
                    id="outlined-multiline-static"
                    label="Career Description"
                    multiline
                    rows={3}
                    inputProps={{ maxLength: 300 }}
                    variant="outlined"
                    style={{ width: "560px", marginLeft: "180px"}}
                />
                <FormHelperText style={{marginBottom: "20px", marginLeft: "180px" }}>300 words</FormHelperText>

                <div className={classes.rates}>
                    <FormControl color="error" className={classes.formControl} style={{marginLeft: "180px", width: "200px",}}>
                        <InputLabel id="demo-simple-select-label" className={classes.inputLabel} style={{fontSize: "14px"}}>Current Occupation</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={currentOccupation}
                            label="Current Occupation"
                            onChange={handleChangeCurrentOccupation}
                        >
                            <MenuItem value={"Unemployed"}>Unemployed</MenuItem>
                            <MenuItem value={"Employed"}>Employed</MenuItem>
                            <MenuItem value={"Freelancer"}>Freelancer</MenuItem>
                            <MenuItem value={"Business Owner"}>Business Owner</MenuItem>
                        </Select>
                        <FormHelperText>Select only one</FormHelperText>
                    </FormControl>

                    <TextField
                        color="error"
                        id="outlined-multiline-static"
                        label="Mission and Vision"
                        multiline
                        rows={2}
                        inputProps={{ maxLength: 100 }}
                        variant="outlined"
                        style={{ width: "200px", marginLeft: "50px"}}
                    />

                    <TextField
                        color="error"
                        id="outlined-multiline-static"
                        label="$ Hourly Rate"
                        multiline
                        rows={1}
                        inputProps={{ maxLength: 20 }}
                        variant="outlined"
                        style={{ width: "200px", marginLeft: "50px"}}
                    />
                    {/* <FormHelperText style={{ marginLeft: "180px" }}>$ Per Hour</FormHelperText> */}
                </div>

                <TextField
                    color="error"
                    id="outlined-multiline-static"
                    label="Self Description"
                    multiline
                    rows={3}
                    inputProps={{ maxLength: 300 }}
                    variant="outlined"
                    style={{ width: "560px", marginLeft: "180px"}}
                />
                <FormHelperText style={{marginBottom: "20px", marginLeft: "180px"}}>300 words</FormHelperText>

            </form>
        </div>
    )
}

export default FormStep2;

