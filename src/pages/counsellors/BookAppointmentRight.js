import React from 'react';
import { makeStyles } from '@mui/styles';
import AccessTimeFilledIcon from '@mui/icons-material/AccessTimeFilled';
import { Stack, TextField } from '@mui/material';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import { Link } from "react-router-dom";
import { LocalizationProvider, TimePicker } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "250px",
    },
    time: {
        display: "flex",
    },
    timestamp: {
        fontSize: "15px",
        color: theme.palette.info.main,
    },
    bookSession: {
        display: "flex",
        marginTop: "30px",
        backgroundColor: theme.palette.info.main,
        padding: "5px",
        width: "150px",
        borderRadius: "25px",
    },
    book: {
        fontSize: "13px",
        color: "#fff",
    },
}))

function BookAppointmentRight() {
    const classes = useStyles();
    const [value, setValue] = React.useState(new Date());

    const handleChange = (newValue) => {
        setValue(newValue);
    }
    // const [time, setTime] = React.useState('');

    // const handleChange = (event) => {
    //     setTime(event.target.value);
    // };

    return (
        <>
        <div className={classes.root}>
            <div className={classes.time}>
                <AccessTimeFilledIcon color="info" style={{ fontSize: "19px", marginRight: "5px" }} />
                <h4 className={classes.timestamp}>Time</h4>
            </div>

            <div>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
                <Stack spacing={3}>
                    <TimePicker
                        color="secondary"
                        size="small"
                        label="Time"
                        value={value}
                        onChange={handleChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    <TimePicker
                        color="secondary"
                        size="small"
                        label="Time"
                        value={value}
                        onChange={handleChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                </Stack>
            </LocalizationProvider>
                {/* <Box>
                    <FormControl color="secondary" size="small" style={{ width: "200px", marginTop: "20px" }}>
                        <InputLabel id="demo-simple-select-label">10 AM</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={time}
                            label="10 AM"
                            onChange={handleChange}
                        >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </Box>
                <Box>
                    <FormControl color="secondary" size="small" style={{ width: "200px", marginTop: "20px" }}>
                        <InputLabel id="demo-simple-select-label">10.20 AM</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={time}
                            label="10 AM"
                            onChange={handleChange}
                        >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </Box> */}
            </div>

            <div className={classes.bookSession}>
                <EventAvailableIcon style={{ marginLeft: "20px", marginRight: "10px", fontSize: "18px", color: "#fff", }} />
                <Link to="/bookAppointment" style={{ textDecoration: "none" }}>
                    <h4 className={classes.book}>Book Session</h4>
                </Link>
            </div>
        </div>
        </>
    )
}

export default BookAppointmentRight
