import React from 'react';
import { makeStyles } from '@mui/styles';
import FlagIcon from '@mui/icons-material/Flag';
import AssignmentIcon from '@mui/icons-material/Assignment';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import ShareIcon from '@mui/icons-material/Share';
import { Button } from '@mui/material';
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "1px",
        position: "sticky",
        top: 0,
        height: "100vh",
    },
    flagReport: {
        display: "flex",
        marginTop: "15px",
    },
    report: {
        color: theme.palette.info.main,
        fontSize: "13px",
    },
    tags: {
        fontSize: "13px",
        marginTop: "15px",
        marginBottom: "15px",
    },
    sessions: {
        fontSize: "13px",
        marginTop: "15px",
        marginBottom: "15px",
    },
    sessionsIcon: {
        display: "flex",
        backgroundColor: "whitesmoke",
        padding: "5px",
        width: "150px", 
    },
    eighty: {
        color: theme.palette.info.main,
        fontSize: "13px",
        marginRight: "20px",
    },
    sessionText: {
        color: theme.palette.info.main,
        fontSize: "13px",
    },
    share: {
        display: "flex",
        marginTop: "25px",
    },
    shareProfile: {
        color: theme.palette.info.main,
        fontSize: "13px",
    },
    bookSession: {
        display: "flex",
        marginTop: "25px",
        backgroundColor: theme.palette.info.main,
        padding: "5px",
        width: "150px",
        borderRadius: "25px",
    },
    book: {
        fontSize: "13px",
        color: "#fff",
    },
}))

function ViewProfileCounsellorRight() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.flagReport}>
                <FlagIcon color="info" style={{ marginRight: "5px", fontSize: "18px"  }} />
                <h4 className={classes.report}>Report</h4>
            </div>
            <div>
                <h3 className={classes.tags}>Tags:</h3>
            </div>
            <div style={{ marginBottom: "10px" }}>
                <Link to="" style={{ textDecoration: "none" }}>
                    <Button variant="contained" style={{ maxWidth: '30px', maxHeight: '30px', fontSize: "10px", marginRight: "30px" }}>
                        Abuse
                    </Button>
                </Link>
                <Link to="" style={{ textDecoration: "none" }}>
                    <Button variant="contained" style={{ maxHeight: '30px', fontSize: "10px" }}>
                        Emotional abuse
                    </Button>
                </Link>
            </div>
            <div>
                <Link to="" style={{ textDecoration: "none" }}>
                    <Button variant="contained" style={{ maxHeight: '30px', fontSize: "10px" }}>
                        Children
                    </Button>
                </Link>
            </div>
            <div>
                <h3 className={classes.sessions}>No. of sessions done:</h3>
            </div>
            <div className={classes.sessionsIcon}>
                <AssignmentIcon color="info" style={{ marginRight: "20px", fontSize: "18px" }} />
                <h4 className={classes.eighty}>80</h4>
                <h4 className={classes.sessionText}>Sessions</h4>
            </div>
            <div className={classes.share}>
                <ShareIcon color="info" style={{ marginRight: "10px", fontSize: "18px" }} />
                <h4 className={classes.shareProfile}>Share Profile</h4>
            </div>
            <div className={classes.bookSession}>
                <EventAvailableIcon style={{ marginLeft: "20px", marginRight: "10px", fontSize: "18px", color: "#fff", }} />
                <Link to="/bookAppointment" style={{ textDecoration: "none" }}>
                    <h4 className={classes.book}>Book Session</h4>
                </Link>
            </div>
        </div>
    )
}

export default ViewProfileCounsellorRight
