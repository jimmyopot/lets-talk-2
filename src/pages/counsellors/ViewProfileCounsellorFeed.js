import React from 'react'
import { makeStyles } from '@mui/styles';
import { Grid, Button } from '@mui/material';
import ViewProfileAboutMe from './components/ViewProfileAboutMe';
import ViewProfileConnect from './components/ViewProfileConnect';
import BookmarkAddedIcon from '@mui/icons-material/BookmarkAdded';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    heading: {
        color: "black",
        marginTop: "20px",
        marginLeft: "10px",
    },
    names: {
        display: "flex",
    },
    name1: {
        color: theme.palette.info.main,
        fontSize: "16px",
        marginTop: "10px",
        marginLeft: "30px",
        marginRight: "200px",
    },
    tags: {
        display: "flex",
        marginTop: "10px",
    },
    tagIcons1: {
        display: "flex",
        backgroundColor: theme.palette.info.main,
        padding: "5px",
        // width: "170px",
        borderRadius: "25px",
    },
    tagIcons2: {
        display: "flex",
        backgroundColor: theme.palette.info.main,
        padding: "5px",
        // width: "150px",
        borderRadius: "25px",
    },
    profileBook1: {
        fontSize: "15px",
        color: "#fff",
        paddingRight: "60px",
    },
    profileBook2: {
        fontSize: "15px",
        color: "#fff",
    },
}))

function ViewProfileCounsellorFeed() {
    const classes = useStyles();

    return (
        <>
            <div className={classes.names}>
                <h3 className={classes.name1}>Dr.Jordan Makulutu</h3>
                <div className={classes.tags}>
                    <div className={classes.tagIcons1}>
                        <BookmarkAddedIcon style={{marginLeft: "20px", marginRight: "5px", color: "#fff", fontSize: "18px"}} />
                        <h4 className={classes.profileBook1}>Save profile</h4>
                    </div>
                    <div className={classes.tagIcons2}>
                        <EventAvailableIcon style={{marginRight: "5px", color: "#fff", fontSize: "18px"}} />
                        <h4 className={classes.profileBook2}>Book session</h4>
                    </div>
                </div>
            </div>

            <Grid container spacing={1} className={classes.gridcontainer3}>
                <Grid item xs={12} sm={12} md={12}>
                    <ViewProfileAboutMe
                        image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                        aboutMe={<h5>About me</h5>}
                        tqe={<h5>Training, qualifications & experience</h5>}
                        memberOrganizations={<h5>Member Organizations</h5>}
                        text={<h6>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                            porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                            ultricies lobortis turpis at pellentesque. 
                        </h6>}

                    />
                </Grid>
            </Grid>

        <div className={classes.heading}>
            <h1 style={{fontSize: "15px", fontWeight: "700"}}>Connect</h1>
        </div>

        <Grid container spacing={1} className={classes.gridcontainer3}>
            <Grid item xs={12} sm={12} md={12}>
                <ViewProfileConnect
                    title={<h3>Dr. Jordan Makulutu</h3>}
                    time={<h6>2 min ago...</h6>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h5>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque. Nunc a sodales leo. Donec odio sem, egestas vel gravida 
                        at, tempus ac nulla. Suspendisse risus sem, venenatis sed dictum non, maximus nec nunc. Fusce 
                        finibus eu enim ultrices luctus. Suspendisse at eros condimentum, euismod nisl vitae, iaculis nulla. 
                        Quisque sit amet lorem erat. Aenean volutpat neque a urna malesuada dapibus.
                    </h5>}
                    readMore={<h5>Read more...</h5>}
                    icon1={<h5>23</h5>}
                    icon2={<h5>100</h5>}
                    icon3={<h5>Share</h5>}
                    icon4={<h5>Follow</h5>}
                />
            </Grid>
        <Grid />
        <Grid container spacing={1} className={classes.gridcontainer3}></Grid>
            <Grid item xs={12} sm={12} md={12}>
                <ViewProfileConnect
                    title={<h3>Pst. Duke Masinde</h3>}
                    time={<h6>2 min ago...</h6>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h5>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque.
                    </h5>}
                    readMore={<h5>Read more...</h5>}
                    icon1={<h5>23</h5>}
                    icon2={<h5>100</h5>}
                    icon3={<h5>Share</h5>}
                    icon4={<h5>Follow</h5>}
                />
            </Grid>
            <div>
            <Link to="" style={{ textDecoration: "none" }}>
                <Button 
                    color="info" 
                    variant="contained"
                    style={{ marginLeft: "300px" }}
                >
                    View more
                </Button>
            </Link>
        </div>
        </Grid>
        </>
    )
};

export default ViewProfileCounsellorFeed;
