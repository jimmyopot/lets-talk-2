import React from 'react'
import { makeStyles } from '@mui/styles';
import { Grid, InputBase } from '@mui/material';
import { Search } from '@mui/icons-material';
import FilterFavoriteCounsellors from './components/FilterFavouriteCounsellors';

const useStyles = makeStyles((theme) => ({
    search: {
        display: "flex",
        marginTop: "30px",
        marginLeft: "10px",
        borderRadius: "25px",
    },

    searchInput: {
        backgroundColor: "gray",
        width: "720px",
        height: "40px",
        padding:" 15px",
        border:" none",
        [theme.breakpoints.down("sm")]: {
            width: "300px",
        },
    },

    searchIcon: {
        backgroundColor: "gray",
        width: "100px",
        height: "20px",
        padding:" 8px",
        border:" none",
        color: "white",
    },
    heading: {
        color: "black",
        marginTop: "20px",
        marginLeft: "10px",
    },
}))

function FilterFeed() {
    const classes = useStyles();

    return (
        <>
        <div className={classes.search}>
            <InputBase placeholder="Find the right counsellor for you" className={classes.searchInput} style={{color: "white"}} />
            <Search className={classes.searchIcon} />
        </div>

        <Grid container spacing={1} className={classes.gridcontainer3}>
            <Grid item xs={12} sm={12} md={12}>
                <FilterFavoriteCounsellors
                    title={<h3>Dr. Jordan Makulutu</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h6>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque. 
                    </h6>}
                    viewProfile={<h5>View Profile</h5>}
                    report={<h5>Report</h5>}
                    tags={<h5>Tags:</h5>}
                    tag1={<h6>Abuse</h6>}
                    tag2={<h6>Emotional abuse</h6>}
                    tag3={<h6>Children</h6>}
                    icon1={<h4>Book session</h4>}
                    icon2={<h4>Save profile</h4>}
                />
            </Grid>
        <Grid />
        <Grid container spacing={1} className={classes.gridcontainer3}></Grid>
            <Grid item xs={12} sm={12} md={12}>
                <FilterFavoriteCounsellors
                    title={<h3>Pst. Duke Masinde</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h6>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque.
                    </h6>}
                    viewProfile={<h5>View Profile</h5>}
                    report={<h5>Report</h5>}
                    tags={<h5>Tags:</h5>}
                    tag1={<h6>Abuse</h6>}
                    tag2={<h6>Emotional abuse</h6>}
                    tag3={<h6>Adults</h6>}
                    icon1={<h4>Book session</h4>}
                    icon2={<h4>Save profile</h4>}
                />
            </Grid>
            <Grid container spacing={1} className={classes.gridcontainer3}></Grid>
            <Grid item xs={12} sm={12} md={12}>
                <FilterFavoriteCounsellors
                    title={<h3>Hope Group</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h6>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque.
                    </h6>}
                    viewProfile={<h5>View Profile</h5>}
                    report={<h5>Report</h5>}
                    tags={<h5>Tags:</h5>}
                    tag1={<h6>Abuse</h6>}
                    tag2={<h6>Emotional abuse</h6>}
                    tag3={<h6>Adults</h6>}
                    icon1={<h4>Book session</h4>}
                    icon2={<h4>Save profile</h4>}
                />
            </Grid>
            <Grid container spacing={1} className={classes.gridcontainer3}></Grid>
            <Grid item xs={12} sm={12} md={12}>
                <FilterFavoriteCounsellors
                    title={<h3>Dr. Jordan Peterson</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h6>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque.
                    </h6>}
                    viewProfile={<h5>View Profile</h5>}
                    report={<h5>Report</h5>}
                    tags={<h5>Tags:</h5>}
                    tag1={<h6>Abuse</h6>}
                    tag2={<h6>Emotional abuse</h6>}
                    tag3={<h6>Adults</h6>}
                    icon1={<h4>Book session</h4>}
                    icon2={<h4>Save profile</h4>}
                />
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
                <FilterFavoriteCounsellors
                    title={<h3>Dr. Jordan Peterson</h3>}
                    image="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjZ8fHBlb3BsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
                    text={<h6>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta nulla quam, nec maximus nibh 
                        porta sed. Sed quis ullamcorper quam, sollicitudin congue dolor. Morbi ut nulla metus. Nunc 
                        ultricies lobortis turpis at pellentesque.
                    </h6>}
                    viewProfile={<h5>View Profile</h5>}
                    report={<h5>Report</h5>}
                    tags={<h5>Tags:</h5>}
                    tag1={<h6>Abuse</h6>}
                    tag2={<h6>Emotional abuse</h6>}
                    tag3={<h6>Adults</h6>}
                    icon1={<h4>Book session</h4>}
                    icon2={<h4>Save profile</h4>}
                />
            </Grid>
        </Grid>

        </>
    )
}

export default FilterFeed;
