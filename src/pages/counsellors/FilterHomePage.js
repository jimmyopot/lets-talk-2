import React from 'react';
import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import FilterLeft from './FilterLeft';
import FilterFeed from './FilterFeed';
import FilterRight from './FilterRight';

const useStyles = makeStyles((theme) => ({
    right: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        },
        [theme.breakpoints.down("md")]: {
            display: "none"
        }
    }
}))

function FilterHomePage() {
    const classes = useStyles();
    
    return (
        <div>
             <Grid container>
                <Grid item sm={2} xs={2}>
                    <FilterLeft />
                </Grid>
                <Grid item sm={7} xs={10}>
                    <FilterFeed />
                </Grid>
                <Grid item sm={3} className={classes.right}>
                    <FilterRight />
                </Grid>
            </Grid>
        </div>
    )
}

export default FilterHomePage;
