import React from 'react';
// import Message from './Message';
// import Progress from './Progress';
// import axios from 'axios';
// import { TextField } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: "150px",
    },
}))

function FileUpload() {
    const classes = useStyles();

    return (
        <form className={classes.root}>
            <div>
                <input
                    type='file'
                    id='customFile'
                    className=''
                />
                <label 
                    className=""
                    htmlfor="customFile"
                >
                    Choose File
                </label>
            </div>
            <input
                type='submit'
                value='Upload'
                className=''
            />
        </form>
            
    );
}

export default FileUpload;