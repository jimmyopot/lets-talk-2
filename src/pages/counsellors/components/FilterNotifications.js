import React from 'react'
import { makeStyles } from '@mui/styles';
import { Card, CardContent } from '@mui/material';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
        display: "flex",
        flexDirection: "column",
    },
    card: {
        display: "flex",
        width: "300px",
        height: "50px",
        marginLeft: "10px",
    },
    content: {
        display: "flex",
        justifyContent: "space-between",
        marginTop: "0px",
    },
    heading: {
        display: "flex",
        fontSize: "5px",
        color: "black",
        fontWeight: "bold",
    },
    icons: {
        display: "flex",
        marginTop: "0px",
        marginLeft: "220px",
    },
    text: {
        fontSize: "12px",
        marginLeft: "70px",
    }
}))

function FilterNotifications( { icon, title, text } ) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.card} style={{backgroundColor: "whitesmoke"}}>
                <CardContent className={classes.content}>
                    <div className={classes.heading}>
                        <p style={{ color: "gray" }}>{icon}</p>
                        <p style={{ fontSize: "12px", color: "gray", marginLeft: "5px" }}>{title}</p>
                    </div>
                    <div >
                        <Link to="/" style={{ color: "blue" }}>
                            <p className={classes.text}>{text}</p>
                        </Link>
                    </div>
                    
                </CardContent>
            </Card>
            
        </div>
    )
}

export default FilterNotifications;
