import * as React from 'react';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  textField: {
    color: theme.palette.error.main,
    width: "200px"
  },
}))

export default function BasicDatePicker() {
  const classes = useStyles();
  const [value, setValue] = React.useState(null);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} clasName={classes.root}>
      <DatePicker
        label="Date of Birth"
        value={value}
        onChange={(newValue) => {
          setValue(newValue);
        }}
        renderInput={(params) => <TextField {...params} color="error" style={{width: "560px", marginBottom: "30px", marginLeft: "180px"}} />}
      />
    </LocalizationProvider>
  );
}
