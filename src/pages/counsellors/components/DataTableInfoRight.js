import React from 'react';
import { DataGrid } from '@mui/x-data-grid';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'firstName', headerName: 'First name', width: 200 },
  { field: 'lastName', headerName: 'Last name', width: 200 },
  { field: 'userName', headerName: 'Username', width: 200 },
  { field: 'email', headerName: 'Email', width: 200 },
  { field: 'dateOfBirth', headerName: 'Date of Birth', type: 'number', width: 200 },
  { field: 'gender', headerName: 'Gender', width: 200 },
  { field: 'language', headerName: 'Language', width: 200 },
  { field: 'country', headerName: 'Country', width: 200 },
];

const rows = [
    { id: 1, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 2, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 3, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 4, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 5, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 6, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 7, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 8, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 9, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
    { id: 10, firstName: 'Jon', lastName: 'Snow', userName: 'Jon Snow', email: 'jonsnow@gmail.com', dateOfBirth: '02/12/90', gender: 'Male', language: 'English', country: 'Britain'},
  ];

export default function DataTableInfoRight() {
  // const [tableData, setTableData] = useState([]);

  // useEffect(() => {
  //   fetch("")
  //   .then((data) => data.json())
  //   .then((data) => setTableData(data))
  // })

  return (
    <div style={{ height: 530, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={8}
        rowsPerPageOptions={[8]}
        checkboxSelection
      />
    </div>
  );
}
