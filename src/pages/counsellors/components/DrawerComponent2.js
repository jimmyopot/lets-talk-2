import React, { useState } from 'react';
import { Drawer, List, ListItem, ListItemText } from '@mui/material';
import { makeStyles } from '@mui/styles';
import MenuIcon from '@mui/icons-material/Menu';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({

    menuIcon: {
        color: "gray",
        marginLeft: "350px",
        width: "50px",
    },
    listContainer: {
        backgroundColor: "whitesmoke",
    },
    listButton: {
        fontSize: "15px",
        color: "blue",
        textDecoration: "none",
        marginLeft: "30px",
        marginRight: "30px",
    },
}))

function DrawerComponent() {
    const classes = useStyles();
    const [openDrawer, setOpenDrawer] = useState(false);
    
    return (
        <>
            <Drawer
                anchor="left"
                open={openDrawer}
                onClose={() => setOpenDrawer(false)}
            >
                <List className={classes.listContainer}>
                    <ListItem divider button>
                        <Link to="/" className={classes.listButton}><ListItemText>Home</ListItemText></Link>
                    </ListItem>
                    
                    <ListItem divider button>
                        <Link to="/getstarted" className={classes.listButton}><ListItemText>Sign Up</ListItemText></Link>
                    </ListItem>
                    <ListItem divider button>
                        <Link to="/login" className={classes.listButton}><ListItemText>Login</ListItemText></Link>
                    </ListItem>
                </List>
            </Drawer>

            <MenuIcon onClick={() => setOpenDrawer(!openDrawer)} className={classes.menuIcon} />
        </>
    )
}

export default DrawerComponent;
