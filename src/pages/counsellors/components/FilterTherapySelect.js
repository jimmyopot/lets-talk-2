import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
    },
}))

export default function FilterTherapySelect() {
    const classes = useStyles();
    const [therapy, setTherapy] = React.useState('');

  const handleChange = (event) => {
    setTherapy(event.target.value);
  };

  return (
    <Box style={{ minWidth: 120 }} className={classes.root}>
      <FormControl fullWidth color="secondary" size="small">
        <InputLabel id="demo-simple-select-label" color="error">Type of Therapy</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={therapy}
          label="Type of Therapy"
          onChange={handleChange}
        >
          <MenuItem value={10}>Psychoanalysis</MenuItem>
          <MenuItem value={20}>Behavior</MenuItem>
          <MenuItem value={30}>Cognitive</MenuItem>
          <MenuItem value={30}>Humanistic</MenuItem>
          <MenuItem value={30}>Integrative</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
