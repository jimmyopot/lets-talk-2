import React from 'react';
import { makeStyles } from '@mui/styles';
import { Card, CardContent, CardMedia } from '@mui/material';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "30px",
        display: "flex",
        marginLeft: "100px",
        // width: "750px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
        },
    },
    cardleft: {
        display: "flex",
        flexDirection: "column",
        width: "700px",
        height: "500px",
        marginLeft: "40px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "420px",
            width: "300px",
            height: "120px",
            border: "none",
            boxShadow: "none",
        },
    },
    title1: {
        fontSize: "25px", 
        marginLeft: "50px",
        marginTop: "50px",
        color: theme.palette.info.main,
    },
    title2: {
        fontSize: "25px", 
        marginLeft: "50px",
        marginTop: "-10px",
        color: theme.palette.error.main,
    },
    title3: {
        display: "flex",
        fontSize: "25px", 
        marginLeft: "50px",
        marginTop: "-10px",
        color: theme.palette.info.main,
    },
    icon: {
        color: theme.palette.secondary.main,
    },
    bodytext: {
        fontSize: "20px", 
        marginTop: "15px", 
        marginRight: "200px",
        marginLeft: "50px",
        color: theme.palette.info.main,
    },
    button: {
        display: "flex",
        marginTop: "15px", 
        marginRight: "200px",
        marginLeft: "50px",
    },
    button2: {
        marginLeft: "180px",
    },
    image: {
        height: '50%',
        paddingTop: "120px",
    },
}))

function CounsellorsHome( {title1, title2, title3, text, button1,  button2, image} ) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.cardleft} style={{ backgroundColor: "#E1F3FE" }}>            
                <CardContent className={classes.cardcontent}>
                    <div className={classes.title1}>
                        <p>{title1}</p>
                    </div>
                    <div className={classes.title2}>
                        <p>{title2}</p>
                    </div>
                    <div className={classes.title3}>
                        <p>{title3}</p>
                        <MoreHorizIcon style={{ fontSize: "35px", marginTop: "20px" }} className={classes.icon} />
                    </div>
                    <div className={classes.bodytext}>
                        <p>{text}</p> 
                    </div>
                    <div className={classes.button}>
                        <p>{button1}</p>
                        <p className={classes.button2}>{button2}</p>
                    </div>
                </CardContent>
            </Card>

            <Card className={classes.cardright} style={{backgroundColor: "white"}}>
                <CardMedia
                    component="img"
                    image={image}
                    alt="doc"
                    className={classes.image}
                />
            </Card>

        </div>
    )
}

export default CounsellorsHome
