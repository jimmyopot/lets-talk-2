import { AppBar, Button, Toolbar, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import Rectangle from "../../../images/rounded-logo.png";
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import DrawerComponent2 from './DrawerComponent2';

import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  logo: {
    maxWidth: 40,
    marginRight: "10px",
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    }
  },

  logoTitle: {
    fontSize: "20px",
    color: theme.palette.primary.main2,
    textDecoration: "none",
  },
  midbarText: {
    marginRight: "25px",
    fontSize: "13px",
    color: theme.palette.primary.main2,
    textDecoration: "none",
  },
  hometext: {
    color: theme.palette.error.main,
    fontSize: "16px",
  },

  rightbar: {
    display: "flex",
    marginLeft: "900px",
  },

  rightbarText: {
    marginRight: "25px",
    fontSize: "13px",
    color: theme.palette.primary.main2,
    textDecoration: "none",
  },

  rightbarTextDonate: {
    padding: "5px",
    borderRadius: "30px",
    backgroundColor: "lightgrey",
    color: theme.palette.primary.main2,
    marginRight: "15px",
    fontSize: "13px",
    textDecoration: "none",
  },

  rightbarText3: {
    marginRight: "25px",
    fontSize: "13px",
    backgroundColor: theme.palette.secondary.main,
    padding: "5px",
    borderRadius: "30px",
    color: theme.palette.primary.main2,
    textDecoration: "none",
  },

  rightbarText2: {
    marginRight: "25px",
    fontSize: "13px",
    color: theme.palette.secondary.main,
    textDecoration: "none",
    padding: "5px",
    borderRadius: "30px",
    backgroundColor: theme.palette.info.main,
  },
  rightbarBadge: {
    marginRight: "25px",
  },
  helpIcon: {
    color: theme.palette.primary.main2,
    marginRight: "25px", 
    marginLeft: "25px",
  }

}))

function NavbarCounsellor(props) {
  const classes = useStyles();

  // breakpoints
  const themeBreakPoint = useTheme();

  const isMatch = useMediaQuery(themeBreakPoint.breakpoints.down('sm'));

  return (
    <div className={classes.root}>
    <AppBar position="static">
      <Toolbar>
        {/* logo and title */}
        <Link to="/">
          <img src={Rectangle} alt="round rectangle" className={classes.logo} />
        </Link>
        <Typography variant="h6">
          <Link to="/" className={classes.logoTitle}>
            Let's Talk
          </Link>
        </Typography>

        { isMatch ? (<DrawerComponent2 />) :
          (<>
            {/* right section */}
            <Typography>
              <div className={classes.rightbar}>
                <Link to="/counsellorHomepage" className={classes.midbarText}><h3 className={classes.hometext}>Homepage</h3></Link> 
                <Button 
                  variant="outlined" 
                  onClick={props.openPopup} 
                  style={{color: "white", backgroundColor: "gray", textTransform: "capitalize", marginRight: "25px", width: "50px", fontSize: "13px"}}
                >
                  Register
                </Button>
                {/* <Link to="/counsellorHomepage" className={classes.midbarText}><h3 className={classes.hometext}>Homepage</h3></Link>  */}
                <Link to="/counsellorLogin" className={classes.rightbarText2}><h3>Login</h3></Link>
              </div> 
            </Typography>
          </>)
        }
        
        

      </Toolbar>
    </AppBar>
    </div>
  )
}

export default NavbarCounsellor;