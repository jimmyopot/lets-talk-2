import React from 'react'
import { makeStyles } from '@mui/styles';
import { Avatar, Card, CardContent, CardMedia } from '@mui/material';
import TextsmsIcon from '@mui/icons-material/Textsms';
import ShareIcon from '@mui/icons-material/Share';
import AddBoxIcon from '@mui/icons-material/AddBox';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
        marginBottom: "50px",
        marginLeft: "20px",
        marginRight: "20px",
        display: "flex",
        flexDirection: "column",
        width: "750px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
        },
    },
    header: {
        marginLeft: "40px",
        marginBottom: "20px",
        display: "flex",
        justifyContent: "space-between"
    },
    avatarTitle: {
        display: "flex",
    },
    card: {
        display: "flex",
        // marginLeft: "20px",
    },
    cardleft: {
        display: "flex",
        flexDirection: "column",
        width: "250px",
        height: "200px",
        marginLeft: "40px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "420px",
            width: "300px",
            height: "120px",
            border: "none",
            boxShadow: "none",
        },
    },
    image: {
        marginTop: "10px",
        marginLeft: "15px",
        padding: "10px",
        height: "150px",
        borderRadius: "100px",
        [theme.breakpoints.down("sm")]: {
            paddingTop: "0px",
            padding: "100px",
            width: "100px",
            height: "100px",
        },
    },
    cardright: {
        width: "700px",
        height: "200px",
        marginRight: "30px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "385px",
            marginTop: "3px",
            width: "350px",
        },
    },
    text: {
        color: theme.palette.info.main,
        fontSize: "15px",
        marginBottom: "15px",
    },
    readMore: {
        fontSize: "15px", 
        color: theme.palette.info.main,
    },
    tagContainer: {
        display: "flex",
        marginLeft: "40px",
        marginTop: "10px",
    },
    icon: {
        display: "flex",
    },
}))

function ViewProfileConnect( { title, time, image, text, readMore, icon1, icon2, icon3, icon4 } ) {
    const classes = useStyles();

    return (
        <>
        <div className={classes.root} style={{ backgroundColor: "whitesmoke" }}>
            <div className={classes.header}>
                <div className={classes.avatarTitle}>
                    <Avatar
                        alt="Remy Sharp"
                        src="/static/images/avatar/1.jpg"
                        sx={{ width: 24, height: 24, marginTop: "20px" }}
                    />
                    <p style={{ fontSize: "13px", marginLeft: "15px", marginTop: "20px" }}>{title}</p>
                </div>
                <p style={{ marginTop: "20px", marginRight: "40px" }}>{time}</p>
            </div>

            <div className={classes.card}>
                <Card className={classes.cardleft} style={{backgroundColor: "white"}}>
                    <CardMedia
                        component="img"
                        image={image}
                        alt="doc"
                        style={{ width: 120 }}
                        className={classes.image}
                    />
                </Card>

                <Card className={classes.cardright}>            
                    <CardContent className={classes.cardcontent}>
                        <div className={classes.text}>
                            {text}
                        </div>
                        <div className={classes.readMore}>
                            <p className={classes.viewprofile}>{readMore}</p>
                        </div>
                    </CardContent>
                </Card>        
            </div>
            <div className={classes.tagContainer}>
                <div className={classes.icon}>
                    <TextsmsIcon style={{ fontSize: "20px", marginRight: "5px", color: "gray" }} />
                    <p style={{marginRight: "20px"}}>{icon1}</p>
                </div>
                <p className={classes.icon2} style={{marginRight: "20px"}}>{icon2}</p>
                <div className={classes.icon}>
                    <ShareIcon style={{ fontSize: "20px", marginRight: "5px", color: "gray" }} />
                    <p className={classes.icon3} style={{marginRight: "20px"}}>{icon3}</p>
                </div>
                <div className={classes.icon}>
                    <AddBoxIcon style={{ fontSize: "20px", marginRight: "5px", color: "gray" }} />
                    <p className={classes.icon4}>{icon4}</p>
                </div>
            </div>
        </div>
        </>
    )
}

export default ViewProfileConnect;
