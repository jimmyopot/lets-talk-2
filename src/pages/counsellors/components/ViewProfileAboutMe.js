import React from 'react'
import { makeStyles } from '@mui/styles';
import { Card, CardContent, CardMedia } from '@mui/material';
import StarPurple500Icon from '@mui/icons-material/StarPurple500';
import HearingIcon from '@mui/icons-material/Hearing';
import BookmarkAddedIcon from '@mui/icons-material/BookmarkAdded';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
// import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
        display: "flex",
        width: "750px",
        [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
        },
    },
    cardleft: {
        display: "flex",
        flexDirection: "column",
        width: "300px",
        height: "330px",
        marginLeft: "30px",
        [theme.breakpoints.down("sm")]: {
            marginRight: "420px",
            width: "300px",
            height: "120px",
            border: "none",
            boxShadow: "none",
        },
    },
    image: {
        marginTop: "10px",
        padding: "10px",
        height: "150px",
        borderRadius: "100px",
        [theme.breakpoints.down("sm")]: {
            paddingTop: "0px",
            padding: "100px",
            width: "100px",
            height: "100px",
        },
    },
    imageIcons: {
        display: "flex",
        flexDirection: "column",
        marginLeft: "60px",
        color: theme.palette.info.main,
    },

    cardright: {
        display: "flex",
        flexDirection: "column",
    },
    aboutMe: {
        fontSize: "18px",
        color: "#111",
        marginBottom: "10px",
    },
    tqe: {
        fontSize: "18px",
        color: "#111",
        marginBottom: "10px",
    },
    memberOrganizations: {
        fontSize: "18px",
        color: "#111",
        marginBottom: "10px",
    },
    text: {
        fontSize: "16px",
        color: theme.palette.info.main,
        marginBottom: "10px",
    },
    tags: {
        display: "flex",
        marginTop: "15px",
    },
    tagIcons1: {
        display: "flex",
        // backgroundColor: "yellow"
    },
    tagIcons2: {
        display: "flex",
        // backgroundColor: "green"
    },
    profileBook1: {
        fontSize: "15px",
        color: theme.palette.info.main,
        paddingRight: "60px",
    },
    profileBook2: {
        fontSize: "15px",
        color: theme.palette.info.main,
    },
}))

function ViewProfileAboutMe( { image, aboutMe, tqe, memberOrganizations, text } ) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.cardleft} style={{backgroundColor: "white"}}>
                <CardMedia
                    component="img"
                    image={image}
                    alt="doc"
                    style={{ width: 120 }}
                    className={classes.image}
                />
                <div className={classes.imageIcons}>
                    <StarPurple500Icon style={{ marginBottom: "15px", marginTop: "10px" }} />
                    <HearingIcon />
                </div>
            </Card>

            <Card className={classes.cardright}>            
                <CardContent className={classes.cardcontent}>
                    <div>
                        <p className={classes.aboutMe}>{aboutMe}</p>
                        <p className={classes.text}>{text}</p>
                    </div>
                    <div>
                        <p className={classes.tqe}>{tqe}</p>
                        <p className={classes.text}>{text}</p>
                    </div>
                    <div>
                        <p className={classes.memberOrganizations}>{memberOrganizations}</p>
                        <p className={classes.text}>{text}</p>
                    </div>
                    <div className={classes.tags}>
                        <div className={classes.tagIcons1}>
                            <BookmarkAddedIcon style={{marginRight: "5px"}} />
                            <h4 className={classes.profileBook1}>Save profile</h4>
                        </div>
                        <div className={classes.tagIcons2}>
                            <EventAvailableIcon style={{marginRight: "5px"}} />
                            <h4 className={classes.profileBook2}>Book session</h4>
                        </div>
                    </div>
                    {/* <div className={classes.tags}>
                        <div className={classes.tagIcons1}>
                            <BookmarkAddedIcon style={{marginRight: "5px"}} />
                            <h4 className={classes.profileBook1}>Save profile</h4>
                        </div>
                        <div className={classes.tagIcons2}>
                            <EventAvailableIcon style={{marginRight: "5px"}} />
                            <h4 className={classes.profileBook2}>Book session</h4>
                        </div>
                    </div> */}
                    
                </CardContent>
            </Card>
            
        </div>
    )
}

export default ViewProfileAboutMe;
