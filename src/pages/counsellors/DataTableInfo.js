import React from 'react';
import { makeStyles } from '@mui/styles';
import { Grid } from '@mui/material';
import DataTableInfoRight from './components/DataTableInfoRight';
import DataTableInfoLeft from './components/DataTableInfoLeft';

const useStyles = makeStyles((theme) => ({

}))

function DataTable() {
    const classes = useStyles();

    return (
        <div>
            <div className={classes.root}>
                <Grid container>
                    <Grid item sm={2} xs={2}>
                        <DataTableInfoLeft />
                    </Grid>
                    <Grid item sm={10} xs={10}>
                        <DataTableInfoRight />
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

export default DataTable
