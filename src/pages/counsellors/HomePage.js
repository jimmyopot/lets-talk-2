import React from "react";
import { makeStyles } from "@mui/styles";
import { Grid } from "@mui/material";
import CounsellorsHome from "./components/CounsellorsHome";
import counsel from "../../images/counsel.jpg";

const useStyles = makeStyles((theme) => ({}));

function HomePage(props) {
  const classes = useStyles();

  return (
    <div>
      <Grid container spacing={1} className={classes.gridcontainer3}>
        <Grid item xs={12} sm={12} md={12}>
          <CounsellorsHome
            title1={<h3>This is the</h3>}
            title2={<h1>Counsellors</h1>}
            title3={<h2>HomePage</h2>}
            text={
              <h6>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
                quis lacus magna. Vivamus eget rutrum mauris. Aenean egestas
                maximus mauris, sed vulputate erat. Mauris nec erat ipsum.
                Aenean consequat risus et enim sollicitudin lacinia. Aliquam
                eget libero quis turpis porta ultricies. Curabitur mi lacus,
                scelerisque sed magna id, fringilla pulvinar justo. Phasellus
                ultrices sollicitudin tincidunt.
              </h6>
            }
            image={counsel}
          />
          
        </Grid>
      </Grid>
    </div>
  );
}

export default HomePage;
