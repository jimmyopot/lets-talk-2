import React from 'react';
import { makeStyles } from '@mui/styles';
import { Grid } from '@mui/material';
import LandingPageLeft from './LandingPageLeft';
import LandingPageRight from './LandingPageRight';

const useStyles = makeStyles((theme) => ({

}))

function LandingPage() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item sm={2} xs={2}>
                    <LandingPageLeft />
                </Grid>
                <Grid item sm={10} xs={10}>
                    <LandingPageRight />
                </Grid>
            </Grid> 
        </div>
    )
}

export default LandingPage
