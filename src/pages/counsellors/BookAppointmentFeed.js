// import React from 'react';
// import DatePicker from 'react-datetime';
// import moment from 'moment';
// import 'react-datetime/css/react-datetime.css';
 
// function BookAppointmentFeed() {
 
//   // disable past dates
//   const yesterday = moment().subtract(1, 'day');
//   const disablePastDt = current => {
//     return current.isAfter(yesterday);
//   };
 
//   // disable future dates
//   const today = moment();
//   const disableFutureDt = current => {
//     return current.isBefore(today)
//   }
 
//   return (
//     <div className="App">
//       <h2>Disable dates in react-datetime - CodeCheef</h2>
 
//       <p className="title">Disable past dates:</p>
//       <DatePicker
//         timeFormat={false}
//         isValidDate={disablePastDt}
//       />
 
//       <p className="title">Disable future dates:</p>
//       <DatePicker
//         timeFormat={false}
//         isValidDate={disableFutureDt}
//       />
//     </div>
//   );
// }
 
// export default BookAppointmentFeed;










import React, { useState } from 'react';
import { makeStyles } from '@mui/styles';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
// import moment from 'moment'
// import BookCalendarSearch from './BookCalendarSearch';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "250px",
        marginLeft: "40px",
    },
    valueResult: {
        marginLeft: "20px",
        marginTop: "10px",
    },
}))

function BookAppointmentFeed() {
    const classes = useStyles();
    const [value, onChange] = useState(new Date());

    // const yesterday = moment().subtract(1, 'day');
    // const disablePastDt = (current) => {
    //     return current.isAfter(yesterday);
    // };

    return (
        <>
        {/* <BookCalendarSearch /> */}
        <div className={classes.root}>
            <Calendar
                onChange={onChange}
                value={value}
                // isValidDate={disablePastDt}
                // minDate
            />
            
        </div>
        <div className={classes.valueResult}>
            {value.toString()}
        </div>
        </>
    )
}

export default BookAppointmentFeed
