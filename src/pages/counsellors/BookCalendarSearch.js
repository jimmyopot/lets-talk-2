// import React from 'react';
// import DatePicker from 'material-ui/DatePicker';

// function disablePrevDates(startDate) {
//   const startSeconds = Date.parse(startDate);
//   return (date) => {
//     return Date.parse(date) < startSeconds;
//   }
// }

// class BookCalendarSearch extends React.Component {
    
//     render() {
//         const startDate = new Date();
// //      or:
// //      const startDate = new Date('December 20, 2016');
// //      const startDate = this.state.firstDate;
      
//         return (
//             <div>
//                 <DatePicker 
//                   hintText="Check-in" 
//                   shouldDisableDate={disablePrevDates(startDate)}
//                 />
//             </div>
//         )
//     }
// }

// export default BookCalendarSearch;






import React, { useState } from 'react';
import { makeStyles } from '@mui/styles';
import { DateRangePicker } from 'react-date-range';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file

const useStyles = makeStyles((theme) => ({
    root: {
        position: "absolute",
        top: "180px",
        left: "25%",
        width: "100vw",
    },
}))

function BookCalendarSearch() {
    const classes = useStyles();
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    
    const selectionRange = {
        startDate: startDate,
        endDate: endDate,
        key: "selection",
    };

    function handleSelect(ranges) {
        setStartDate(ranges.selection.startDate);
        setEndDate(ranges.selection.endDate);
    }

    // function disablePrevDates(startDate) {
    //   const startSeconds = Date.parse(startDate);
    //   return (date) => {
    //     return Date.parse(date) < startSeconds;
    //   }
    // }

    return (
        <div className={classes.root}>
            <DateRangePicker 
                // disabledDates={disablePrevDates(startDate)}
                ranges={[selectionRange]}
                onChange={handleSelect}
            />

        </div>
    )
}

export default BookCalendarSearch
